<!--
Codice da inserire in login quando si vorrà attivare questa funzionalità sbuggata.
<div class="alert alert-info" role="alert">
  <a href="passwordsmarrita.php" class="alert-link">Ho dimenticato la password.</a>
</div>-->
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Recupero Password</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets\bootstrap-3.3.7-dist\css\bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Recupero password</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">
                <form action="invioPassword.php" method="post" autocomplete="true">
                  <fieldset>
                    <div class="form-group row">
                      <label for="email" class="col-form-label col-sm-3">
                        Email
                      </label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Indirizzo Email" aria-described="emailTip" required/>
                        <span id="emailTip" class="help-block">Non condivideremo la sua email con nessuno.</span>
                      </div>
                    </div>
                  </fieldset>
                  <input type="submit" class="btn btn-primary btn-large" value="Invia" id="recupera" name="recupera" />
                </form>
              </div>
              <div class="col-sm-6">
                Digitare nel campo qui affianco l'email dell'account di cui si vuole recuperare la password. Successivamente, cliccare sul bottone Invia per inviare un messaggio contenente un link con il quale si potrà effettuare il cambio della password.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
    Da non modificare ed importare in ogni progetto.
    Deve rimanere sempre alla fine della pagina.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets\bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
</body>
</html>
