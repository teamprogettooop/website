<?php

if(isset($_POST["submit"])){
	$celiaco=0;
	$vegetariano=0;
	$vegano=0;
	$ingredienti=$_POST["hidden-text"];
	$ing=explode(",",$ingredienti);

	$linkpiccolo=$_POST["linkp"];
	$linkgrande=$_POST["linkg"];
	$arraylinkpiccolo=explode(".",$linkpiccolo);
	$arraylinkgrande=explode(".",$linkgrande);
	$nomefilep=$arraylinkpiccolo[0]."piccola.".$arraylinkpiccolo[1];
	$nomefileg=$arraylinkgrande[0]."grande.".$arraylinkgrande[1];
  	$filep = $_FILES['linkp'];
 	$fileg = $_FILES['linkg'];
  	//$target_fileg = $target_dir . basename($_FILES["linkg"]["name"]);
  	//move_uploaded_file($_FILES["linkg"]["tmp_name"], $target_file);
  	//move_uploaded_file($filep[‘tmp_name’], UPLOAD_DIR.$filep[‘name’]);
  	//move_uploaded_file($fileg[‘tmp_name’], UPLOAD_DIR.$fileg[‘name’]);

	if(isset($_POST["celiaco"])){
		$celiaco=1;
	}

	if(isset($_POST["vegetariano"])){
		$vegetariano=1;
	}

	if(isset($_POST["vegano"])){
		$vegano=1;
		$vegetariano=1;
	}

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "my_tentonisanzio";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

  	$target_dir = "assets/imgproducts/";
  	$target_filep = $target_dir . $_FILES["linkp"]["name"];
  	$target_fileg = $target_dir . $_FILES["linkg"]["name"];
	$sql = "INSERT INTO `Prodotto`(`Nome`, `Prezzo`, `Descrizione`, `Categoria`, `Link`, `LinkBig`, `Abilitato`, `Vegetariano`, `Vegano`, `Celiaco`) VALUES ('" . $_POST["nome"] . "','" . $_POST["prezzo"] . "','" . $_POST["descrizione"] . "','" . $_POST["cat"] . "','" . $target_filep . "','" . $target_fileg . "','1','" . $vegetariano . "','" . $vegano . "','" . $celiaco . "')";
	$result=$conn->query($sql);
  	move_uploaded_file($_FILES["linkp"]["tmp_name"], $target_filep);
  	move_uploaded_file($_FILES["linkg"]["tmp_name"], $target_fileg);
  	$fileesitop = " The file ". $_FILES["linkp"]["name"]. " as " . $_FILES["linkp"]["tmp_name"] . " has been uploaded in " . $target_filep . ".";
  	$fileesitog = " The file ". $_FILES["linkg"]["name"]. " as " . $_FILES["linkg"]["tmp_name"] . " has been uploaded in " . $target_fileg . ".";
	$lastid = $conn->insert_id;
	for($i=0;$i<count($ing);$i++){
		$sql = "INSERT INTO `Preparato` (`IdProdotto`,`IdIngrediente`) VALUES (".$lastid.",".$ing[$i].")";
		$result=$conn->query($sql);
	}

	$conn->close();
}

/*function doUpload($file, $upload_dir) {
    global $thatsAll;
	echo "dentro funzione";
    $nomefilep1 = $_FILES['linkp']['tmp_name'];
	$nomefileg1 = $_FILES['linkg']['tmp_name'];
	echo "var dump nomefilep ->";
	var_dump($nomefilep1);
	echo "var dump nomefileg ->";
	var_dump($nomefileg1);
    $nomereale = $file;
    $nomereale = htmlentities(strtolower($nomereale));

	$newname = ($nomereale);
	$ext = end(explode('.',$nomereale));
	$filename = explode('.',$nomereale);
	$newname = str_replace(' ', '_', $newname);
	move_uploaded_file($nomefile,($upload_dir.'/'.$newname));
	echo $thatsAll;
}*/
?>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Registrazione Prodotti</h3>
      </div>
      <div class="panel-body">
		<form method="post" action="#" enctype="multipart/form-data">
			<div class="row">
				<label for="nome" class="col-form-label col-sm-2">Nome:</label>
			  <div class="col-sm-12">
				<input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Prodotto" required/><br/>
			  </div>
			</div>
			<div class="row">
				<label for="prezzo" class="col-form-label col-sm-12">Prezzo:</label>
			  <div class="col-sm-2">
				<input type="number" step='0.01' value='0.00' placeholder='0.00' class="form-control" name="prezzo" id="prezzo" required/><br/>
			  </div>
			</div>
			<div class="row">
			  <div class="col-sm-12">
				<label for="descrizione">Descrizione:</label>
				<textarea name="descrizione" id="descrizione" class="form-control areacontattaci" style="max-width: 100%" placeholder="Descrizione prodotto" required></textarea><br/>
			  </div>
			</div>
			<div class="row">
				<label for="cat" class="col-form-label col-sm-2">Categoria:</label>
			  <div class="col-sm-12">
				<select id="cat" name="cat">
				  <option value="1">Antipasto</option>
				  <option value="2">Primo</option>
				  <option value="3">Secondo</option>
				  <option value="4">Contorno</option>
				  <option value="5">Dolce</option>
				  <option value="6">Frutta</option>
				</select>
			  </div>
			</div>
			<br/>
			<div class="row">
			  <div class="col-sm-12">
				<label for="linkp">Immagine piccola:</label>
				<input type="file" class="form-control" name="linkp" id="linkp" accept="image/*" /><br/>
			  </div>
			</div>
			<div class="row">
			  <div class="col-sm-12">
				<label for="linkg">Immagine grande:</label>
				<input type="file" class="form-control" name="linkg" id="linkg" accept="image/*" /><br/>
			  </div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<input type="checkbox" name="celiaco" id="celiaco" value="Celiaco">	Celiaco<br>
					<input type="checkbox" name="vegetariano" id="vegetariano" value="Vegetariano">	Vegetariano<br>
					<input type="checkbox" name="vegano" id="vegano" value="Vegano"> Vegano<br>
					<br/>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<input type="text" name="hidden-text" id="hidden-text" value=""><br>
					<br/>
				</div>
			</div>
			<table class="table table-condensed table-hover table-bordered" id="tableing">
			  <thead>
				<th>Nome</th>
				<th>Operazioni</th>
			  </thead>
			  <tbody id="agg-ing">

			  </tbody>
			</table>
			<div class= "row">
				<div class="col-sm-12">
					<a class="btn btn-primary" data-toggle="modal" data-target="#modal">Aggiungi ingrediente</a>
				 </div>
				 <br/>
				 <br/>
				 <div class="col-sm-12">
					<input type="submit" name="submit" id="submit" value="Invia" class="btn btn-primary"/>
				 </div>
			</div>
		</form>
    </div>
  </div>
  <div id="modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Inserisci ingredienti a prodotto</h4>
			</div>
			<div class="modal-body">
				<?php require "assets/filepart/aggiungi_ingredienti_al_prodotto.php"; ?>
			</div>
		</div>
	</div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Rimuovi Prodotto</h3>
      </div>
      <div class="panel-body">
        <table class="table table-condensed table-hover table-bordered" id="tableprod">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Operazioni</th>
            </tr>
          </thead>
          <tbody id="tablebody">
            <?php
            $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
            if($conn->connect_errno) {
              $esito = "connerr";
            } else {
              $sql = "SELECT * FROM Prodotto ORDER BY Nome";
              $result = $conn->query($sql);
              while(($row = $result->fetch_array(MYSQLI_ASSOC)) != null) {
                echo "<tr>";
                echo "  <td>" . $row["Nome"] . "</td>";
                echo "  <td><a class='delete-prod-link' href='" . $row['IdProdotto'] . "' ><em id='" . $row['IdProdotto'] . "' class='fa fa-trash-o' aria-hidden='true'></em>&nbsp;Elimina</a></td>";
                echo "</tr>";
              }
              $conn->close();
            }
             ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
