<li class="dropdown">
  <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-fw" aria-hidden="true"></i>&nbsp;Benvenuto <? echo $user["Nome"]; ?><span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="account.php" class="dropdown-item"><i class="fa fa-window-maximize fa-fw" aria-hidden="true"></i>&nbsp;Pannello Utente</a></li>
    <li><a href="carrello.php" class="dropdown-item"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i>&nbsp;Carrello</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="logout_db.php" class="dropdown-item"><i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>&nbsp;Esci</a></li>
  </ul>
</li>
