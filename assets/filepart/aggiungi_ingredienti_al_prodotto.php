<table class="table table-condensed table-hover table-bordered" id="tableing">
  <thead>
    <th>Nome</th>
    <th>Operazioni</th>
  </thead>
  <tbody id="tablebody">
    <?php
    $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
    if($conn->connect_errno) {
      $esito = "connerr";
    } else {
      $sql = "SELECT * FROM Ingrediente ORDER BY Nome";
      $result = $conn->query($sql);
      while(($row = $result->fetch_array(MYSQLI_ASSOC)) != null) {
        echo "<tr>";
        echo "  <td>" . $row["Nome"] . "</td>";
        echo "  <td><a class='add-ing-link' href='" . $row['IdIngrediente'] . "/" . $row['Nome'] . "' ><em id='" . $row['IdIngrediente'] . "' class='fa fa-plus-circle' aria-hidden='true'></em>&nbsp;Aggiungi</a></td>";
        echo "</tr>";
      }
      $conn->close();
    }
     ?>
  </tbody>
</table>
