<?php require_once("constants.php"); ?>
<footer>
	<div class="footer container">
		<div class="row">
			<div class="col-sm-4">
				<ul class="fa-ul">
					<li><a href="index.php"><em class="fa-li fa fa-cutlery fa-fw" aria-hidden="true"></em>&nbsp;Home</a></li>
					<li><a href="prodotti.php"><em class="fa-li fa fa-th-list fa-fw" aria-hidden="true"></em>&nbsp;Prodotti</a></li>
					<?php
						if(isset($_SESSION["login_user"])) {
						$user = $_SESSION["login_user"];
						echo '<li><a href="account.php"><em class="fa-li fa fa-window-maximize fa-fw" aria-hidden="true"></em>&nbsp;Pannello Utente</a></li>
							<li><a href="carrello.php"><em class="fa-li fa fa-shopping-cart fa-fw" aria-hidden="true"></em>&nbsp;Carrello</a></li>
							<li><a href="logout_db.php"><em class="fa-li fa fa-sign-out fa-fw" aria-hidden="true"></em>&nbsp;Esci</a></li>';
						} else {
							echo '<li><a href="register.php"><em class="fa-li fa fa-user-plus fa-fw" aria-hidden="true"></em>&nbsp;Registrati</a></li>
							<li><a href="login.php"><em class="fa-li fa fa-sign-in fa-fw" aria-hidden="true"></em>&nbsp;Login</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="col-sm-4">
				<ul class="fa-ul">
					<li><a href="contattaci.php"><em class="fa-li fa fa-phone fa-fw" aria-hidden="true"></em>&nbsp;Contattaci</a></li>
					<li><a href="doveconsegnamo.php"><em class="fa-li fa fa-map-marker fa-fw" aria-hidden="true"></em>&nbsp;Dove Consegnamo</a></li>
					<li><a href="privacy.php"><em class="fa-li fa fa-lock fa-fw" aria-hidden="true"></em>&nbsp;Privacy e Coockie</a></li>
					<li><a href="team.php"><em class="fa-li fa fa-users fa-fw" aria-hidden="true"></em>&nbsp;Team</a></li>
				</ul>
			</div>
			<div class="col-sm-4">
				<p class="right">&copy; 2020 Help Developers. &middot;</p>
				<p class="right">Capitale Sociale: 150€</p>
				<strong class="right">Partita IVA: 02589580402</strong>
				<address>
				<strong><?php echo($companyname); ?></strong><br>
				Via Violetti 699<br>
				Cesena, 47521, Italy<br>
				Telefono/fax: 0547 645505
				</address>
				<address>
				<strong>E-mail</strong><br>
				<a href="mailto:<?php echo($adminmail); ?>"><?php echo($adminmail); ?></a>
				</address>
			</div>
		</div>
	</div>
</footer>
