<?php require_once("constants.php"); ?>
<header>
  <!--
    Barra di navigazione del sito.
    E' fissata in alto grazie al tag navbar-fixed-top.
    Contiene il Brand e il menù di bottoni.
  --><nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false">
          <span class="sr-only">Mostra e Nasconde la barra di navigazione</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><?php echo($project_name); ?></a>
      </div>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="index.php"><em class="fa fa-cutlery fa-fw" aria-hidden="true"></em>&nbsp;Home</a></li>
          <li><a href="prodotti.php"><em class="fa fa-th-list fa-fw" aria-hidden="true"></em>&nbsp;Prodotti</a></li>
          <?php
            session_start();
            if(isset($_SESSION["login_user"])) {
              $user = $_SESSION["login_user"];
              if($user["Titolo"] == "Amministratore") {
                require "assets/filepart/adminPartNav.php";
              } else {
                require "assets/filepart/userPartNav.php";
              }
            } else {
              echo '<li><a href="register.php"><em class="fa fa-user-plus fa-fw" aria-hidden="true"></em>&nbsp;Registrati</a></li>
              <li><a href="login.php"><em class="fa fa-sign-in fa-fw" aria-hidden="true"></em>&nbsp;Login</a></li>';
            }
          ?>
        </ul>
      </div>
    </div>
  </nav>
  <!--
      Barra dei cookie
    --><script>
	var _iub = _iub || [];
	_iub.csConfiguration = {
		cookiePolicyId: 27984382,
		siteId: 990308,
		lang: "it",
        consentOnScroll: true,
        localConsentDomain: 'tentonisanzio.altervista.org',
        banner: {
            position: "bottom",
            acceptButtonDisplay: true,
            customizeButtonDisplay: true
        }
	};
</script>
<script src="//cdn.iubenda.com/cookie_solution/safemode/iubenda_cs.js" charset="UTF-8" async></script>
</header>
