<?php
if(isset($_SESSION["login_user"])) {
  $esito = "positivo";
  $user = $_SESSION["login_user"];
  $idutente = $user["IdUtente"];
  $prodotti = 0;

  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error) {
    $carrello = "connerr";
  } else {
    $sql = "SELECT COUNT(*) as Numero from Carrello where Carrello.IdUtente = '$idutente'";
    $result = $conn->query($sql);
    while($dati = $result->fetch_array()) {
      $prodotti = $dati["Numero"];
    }
    $conn->close();
  }
} else {
  header("Refresh: 0;URL=login.php");
}
  ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Gestione database</h3>
  </div>
  <ul class="list-group">
    <li class="list-group-item"><a href="account.php?elem=Ingredienti"><i class="fa fa-window-maximize fa-fw" aria-hidden="true"></i>&nbsp; Gestione Ingredienti</a></li>
   	<li class="list-group-item"><a href="account.php?elem=Prodotti"><i class="fa fa-window-maximize fa-fw" aria-hidden="true"></i>&nbsp; Gestione Prodotti&nbsp;</a></li>
	  <li class="list-group-item"><a href="account.php?elem=Consegne"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i>&nbsp; Gestione Consegne&nbsp;</a></li>
    <li class="list-group-item"><a href="account.php?elem=Account"><i class="fa fa-window-maximize fa-fw" aria-hidden="true"></i>&nbsp; Pannello Utente</a></li>
	  <li class="list-group-item">
      <span class="badge"><?php echo $prodotti; ?></span>
      <a href="carrello.php"><em class="fa fa-shopping-cart fa-fw" aria-hidden="true"></em>&nbsp; Carrello&nbsp;</a>
    </li>
    <li class="list-group-item"><a href="logout_db.php"><i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>&nbsp; Logout</a></li>
  </ul>
</div>
