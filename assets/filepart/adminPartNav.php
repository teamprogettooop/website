<li class="dropdown">
  <a href="index.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><em class="fa fa-user fa-fw" aria-hidden="true"></em>&nbsp;Benvenuto <? echo $user["Nome"]; ?><span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="account.php?elem=Ingredienti" class="dropdown-item"><em class="fa fa-window-maximize fa-fw" aria-hidden="true"></em>&nbsp;Gestione Ingredienti</a></li>
	  <li><a href="account.php?elem=Prodotti" class="dropdown-item"><em class="fa fa-window-maximize fa-fw" aria-hidden="true"></em>&nbsp;Gestione Prodotti</a></li>
    <li><a href="account.php?elem=Consegne" class="dropdown-item"><em class="fa fa-shopping-cart fa-fw" aria-hidden="true"></em>&nbsp;Gestione Consegne</a></li>
    <li role="separator" class="divider"></li>
	  <li><a href="account.php?elem=Account" class="dropdown-item"><em class="fa fa-window-maximize fa-fw" aria-hidden="true"></em>&nbsp; Pannello Utente</a></li>
    <li><a href="carrello.php" class="dropdown-item"><em class="fa fa-shopping-cart fa-fw" aria-hidden="true"></em>&nbsp;Carrello</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="logout_db.php" class="dropdown-item"><em class="fa fa-sign-out fa-fw" aria-hidden="true"></em>&nbsp;Esci</a></li>
  </ul>
</li>
