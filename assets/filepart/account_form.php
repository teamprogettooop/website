<form method="post" action="modificaaccount.php" autocomplete="true">
  <fieldset>
    <div class="form-group row">
      <label for="username" class="col-form-label col-sm-2">Nome</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="name" id="name" placeholder="Nome" value="<? echo $user["Nome"]; ?>" required />
        <small id="nameTip" class="help-block">Nome dell'utente. Le firme delle recensioni sul sito conterranno sia il nome che il Cognome.</small>
      </div>
    </div>
    <div class="form-group row">
      <label for="surname" class="col-form-label col-sm-2">Cognome</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="surname" id="surname" value="<? echo $user["Cognome"]; ?>" placeholder="Cognome" required />
        <small id="surnameTip" class="help-block">Cognome dell'utente. Le firme delle recensioni sul sito conterranno sia il nome che il cognome.</small>
      </div>
    </div>
    <div class="form-group row">
      <label for="telephone" class="col-form-label col-sm-2">Telefono</label>
      <div class="col-sm-10">
        <input type="tel" class="form-control" name="telephone" id="telephone" placeholder="Telefono"  value="<? echo $user["Telefono"]; ?>" required />
        <small id="telephoneTip" class="help-block">Telefono dell'utente. Sarà utilizzato dal corriere in caso di necessità.</small>
      </div>
    </div>
    <div class="form-group row">
      <label for="email" class="col-form-label col-sm-2">Indirizzo Email</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<? echo $user["Email"]; ?>" required />
        <small id="emailTip" class="help-block">Non condivideremo la sua email con nessuno.</small>
      </div>
    </div>
    <div class="form-group row">
      <label for="password" class="col-form-label col-sm-2">Password</label>
      <div class="col-sm-10">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" />
        <small id="passwordTip" class="help-block">Password di sicurezza per l'accesso ai servizi.</small>
      </div>
    </div>
    <div class="form-group row">
      <label for="" class="col-form-label col-sm-2">Conferma Password</label>
      <div class="col-sm-10">
        <input type="password" class="form-control" name="conpassword" id="conpassword" placeholder="Conferma Password" />
        <small id="conpassowrdTip" class="help-block">Chiediamo di confermare la password per sicurezza.</small>
      </div>
    </div>
	<?php
		if(get_site_config_by_name("SHOW_PATOLOGIES") == "true") {
			?>
			<fieldset class="form-group">
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
					<div class="form-check">
						<label class="form-check-label" for="celiaco">
						<input class="form-check-input" type="checkbox" name="celiaco" id="celiaco" <?php
						if($user["Celiaco"] == "1") {
							echo "checked";
						}
						?>
						/> Sono Celiaco
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label" for="vegetariano">
						<input class="form-check-input" type="checkbox" name="vegetariano" id="vegetariano"  <?php
						if($user["Vegetariano"] == "1") {
							echo "checked";
						}
						?>
						/> Sono vegetariano
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label" for="vegano">
						<input class="form-check-input" type="checkbox" name="vegano" id="vegano"  <?php
						if($user["Vegano"] == "1") {
							echo "checked";
						}
						?>
						/> Sono vegano
						</label>
					</div>
					<span id="celiacoTip" class="help-block">Non divulgeremo queste informazioni a nessuno.</span>
					</div>
				</div>
			</fieldset>
			<?php
		} else {
			echo "<p class='alert alert-info'>Non sono necessarie ulteriori informazioni.</p>";
		}
	?>
    
    <input type="submit" id="modifica" name="modifica" class="btn btn-primary btn-large" value="Modifica account" />
    <label for="reset" class="sr-only">Azzera tutti i campi</label>
    <input type="reset" class="btn btn-outline-primary" name="reset" id="reset" value="Azzera"/>
  </fieldset>
</form>
