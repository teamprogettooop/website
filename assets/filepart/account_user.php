<?php
	/*
		Aggiornamento delle variabili di sessione con i dati dell'utente.
	*/
  	$conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
	$conn->set_charset("utf8");
	if($conn->connect_errno) {
		$esito = "connerr";
	} else {
		$user = $_SESSION["login_user"];
		$userid = $user["IdUtente"];
		$sql = "SELECT Utente.IdUtente, Nome, Cognome, Titolo, Telefono, Email, Celiaco, Vegetariano, Vegano, ConfEmail FROM Utente INNER JOIN (Potere INNER JOIN Permesso ON Potere.IdPermesso = Permesso.IdPermesso) ON Utente.IdUtente = Potere.IdUtente WHERE Utente.IdUtente = ". $userid . " LIMIT 1";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$user = $result->fetch_array(MYSQLI_ASSOC);
			$_SESSION["login_user"] = $user;
		} else {
			$esito = $conn->errno;
		}
		$conn->close();
	}
?>
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <?php require "pannello_controllo_part.php"; ?>
    </div>
    <div class="col-sm-9">
      <?php
        $user = $_SESSION["login_user"];
      ?>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Modifica informazioni utente</h3>
        </div>
        <div class="panel-body">
          <?php

          /*
          Messaggi di errore di connessione al database durante l'aggiornamento della sessione con i dati dell'utente.
          */
          if(isset($esito)) {
            echo '<div class="alert alert-warning">';
            if($esito === "connerr") {
              echo "Non sono riuscito a connettermi al database per aggiornare le informazioni dell'utente.";
            } else if($esito === "dataerr") {
              echo "Non sono riuscito a recuperare le informazioni dell'utente aggiornate dal database.";
            } else {
              echo "Rilevato errore sconosciuto. Codice errore: " . $esito;
            }
            echo "</div>";
          }

          /*
          Ricordo all'utente di confermare l'email.
          */
          if($user["ConfEmail"] == 0) {
            echo '<div class="alert alert-warning" role="alert">';
            echo "Non hai ancora confermato l'indirizzo email. Trovi il link nell'email che ti è stata inviata all'atto della registrazione. Se non trovi l'email, controlla nella sezione spam, altrimenti ";
            echo '<a class="alert-link" href="confermaemail.php">clicca qui.</a>
            </div>';
          }

          /*
          Messaggi di errore dopo la procedura di modifica account passati in GET.
          */
          if(isset($_GET["coderror"])) {
            if($_GET["coderror"] === "ok") {
              echo '<div class="alert alert-success" role="alert">';
              echo "Operazione completata con successo.";
              echo '</div>';
            } else {
              echo '<div class="alert alert-warning" role="alert">';
              if($_GET["coderror"] === "nopost") {
                echo "Sei stato reindirizzato qui da un'altra pagina.";
              } else if($_GET["coderror"] === "connerr") {
                echo "C'è stato un problema con la connessione al server, riprovare più tardi.";
              } else if($_GET["coderror"] === "passne") {
                echo "Le password non corrispondono, correggere e riprovare.";
              } else if($_GET["coderror"] === "chnpwd") {
                echo "La modifica della password non è andata a buon fine, riprovare più tardi";
              } else {
                echo "Errore sconosciuto, riprovare più tardi o contattare un amministratore di sistema.";
              }
              echo '</div>';
            }
          }
          require "assets/filepart/account_form.php";
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
