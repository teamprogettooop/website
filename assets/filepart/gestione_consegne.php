<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Consegne Pendenti</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <p class="alert alert-info" id="alertmsg">È possibile avanzare le consegne da questa pagina.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="table-responsive">
              <table class="table table-condensed table-hover table-bordered" id="tableing">
                <thead>
                  <tr>
                    <th>Utente</th>
                    <th>DataOrdine</th>
                    <th>Stato Consegna</th>
                    <th>Indirizzo di Consegna</th>
                    <th>Importo Totale</th>
                    <th>Operazioni</th>
                  </tr>
                </thead>
                <tbody id="tablebody">
                  <?php
                  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
                  $conn->set_charset("utf8");
                  if($conn->connect_errno) {
                    $esito = "connerr";
                  } else {
                    $user = $_SESSION["login_user"];
                    $userid = $user["IdUtente"];
                    $sql = "SELECT Utente.Nome AS NomeUtente, DataOrdine, StatoConsegna.Nome AS Stato, IdOrdine, SUM(ImportoUnitario * Quantita) AS Totale, Consegna
                    FROM (Utente JOIN
                          (Storico JOIN StatoConsegna
                            ON Storico.StatoConsegna = StatoConsegna.IdStato)
                            ON Utente.IdUtente = Storico.IdUtente)
                    WHERE Storico.StatoConsegna IN (1, 2)
                    GROUP BY IdOrdine";
                    $result = $conn->query($sql);
                    while(($row = $result->fetch_array(MYSQLI_ASSOC)) != null) {
                      echo "<tr>";
                      echo "  <td>" . $row["NomeUtente"] . "</td>";
                      echo "  <td>" . $row["DataOrdine"] . "</td>";
                      echo "  <td id='stato" . $row['IdOrdine'] . "'>" . $row['Stato'] . "</td>";
                      echo "  <td>" . $row["Consegna"] . "</td>";
                      echo "  <td>" . $row["Totale"] . " €</td>";
                      echo "  <td><a class='consegna-link' href='" . $row["IdOrdine"] . "' ><em id='" . $row["IdOrdine"] . "' class='fa fa-arrow-right' aria-hidden='true'></em>Avanza</a></td>";
                      echo "</tr>";
                    }
                    $conn->close();
                  }
                   ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
