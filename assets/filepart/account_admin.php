<?php
	/*
	Aggiornamento delle variabili di sessione con i dati dell'utente.
	*/
  	$conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
	$conn->set_charset("utf8");
	if($conn->connect_errno) {
		$esito = "connerr";
	} else {
		$user = $_SESSION["login_user"];
		$userid = $user["IdUtente"];
		$sql = "SELECT Utente.IdUtente, Nome, Cognome, Titolo, Telefono, Email, Celiaco, Vegetariano, Vegano, ConfEmail FROM Utente INNER JOIN (Potere INNER JOIN Permesso ON Potere.IdPermesso = Permesso.IdPermesso) ON Utente.IdUtente = Potere.IdUtente WHERE Utente.IdUtente = ". $userid . " LIMIT 1";
		$result = $conn->query($sql);
		if($result->num_rows > 0) {
			$user = $result->fetch_array(MYSQLI_ASSOC);
			$_SESSION["login_user"] = $user;
		} else {
			$esito = $conn->errno;
		}
		$conn->close();
	}
	if($user["Titolo"] != "Amministratore") {
		header("Refresh: 0;URL=account.php");
	}
?>
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <?php require "assets/filepart/pannello_controllo_admin_part.php"; ?>
    </div>
    <div class="col-sm-9">
      <?php
      if(isset($_GET["elem"])) {
        $elem = $_GET["elem"];
        if($elem == "Ingredienti") {
        	require "assets/filepart/gestione_ingredienti.php";
        } else if ($elem == "Consegne") {
        	require "assets/filepart/gestione_consegne.php";
        } else if($elem == "Prodotti") {
        	require "assets/filepart/gestione_prodotti.php";
        } else if($elem == "Account"){
    		require "assets/filepart/account_form.php";
        } else {
          echo "<div class='panel panel-warning'>";
          echo "  <div class='panel-heading'>";
          echo "    <h3 class='panel-title'>Errore nel caricamento della pagina</h3>";
          echo "  </div>";
          echo "  <div class='panel-body'>";
          echo "    C'è stato un problema nel caricamento della pagina, tornare indietro e riprovare, altrimenti contattare un amministratore di sistema.";
          echo "  </div>";
          echo "</div>";
        }
      } else {
        echo "<div class='panel panel-warning'>";
        echo "  <div class='panel-heading'>";
        echo "    <h3 class='panel-title'>In attesa di operazioni</h3>";
        echo "  </div>";
        echo "  <div class='panel-body'>";
        echo "    Selezionare un'operazione dal Pannelli di Controllo qui a fianco.";
        echo "  </div>";
        echo "</div>";
      }
      ?>
    </div>
  </div>
</div>
