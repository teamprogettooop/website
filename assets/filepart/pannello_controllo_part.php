<?php
if(isset($_SESSION["login_user"])) {
  $user = $_SESSION["login_user"];
  $idutente = $user["IdUtente"];
  $prodotti = 0;

  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error) {
    $carrello = "connerr";
  } else {
    $sql = "SELECT COUNT(*) as Numero from Carrello where Carrello.IdUtente = '$idutente'";
    $result = $conn->query($sql);
    while($dati = $result->fetch_array()) {
      $prodotti = $dati["Numero"];
    }
    $conn->close();
  }
} else {
  header("Refresh: 0;URL=login.php");
}
  ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Pannello di Controllo</h3>
  </div>
  <ul class="list-group">
    <li class="list-group-item"><a href="account.php"><em class="fa fa-window-maximize fa-fw" aria-hidden="true"></em>&nbsp; Pannello Utente</a></li>
    <li class="list-group-item">
      <span class="badge"><?php echo $prodotti; ?></span>
      <a href="carrello.php"><em class="fa fa-shopping-cart fa-fw" aria-hidden="true"></em>&nbsp; Carrello&nbsp;</a>
    </li>
    <li class="list-group-item"><a href="logout_db.php"><em class="fa fa-sign-out fa-fw" aria-hidden="true"></em>&nbsp; Logout</a></li>
  </ul>
</div>
