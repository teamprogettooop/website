<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Aggiungi Ingredienti</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <p class="alert alert-info" id="alertmsg">È possibile inserire nuovi ingredienti</p>
          </div>
        </div>
        <form id="aggiungiform" action="aggiungi_ingrediente.php" method="post" autocomplete="true">
          <fieldset>
            <div class="form-group row">
              <label for="nome" class="col-form-label col-sm-3">
                Nome Ingrediente
              </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" aria-described="nomeTip" required/>
                <span id="nomeTip" class="help-block">Nome dell'ingrediente da aggiungere nel database, deve essere esaustivo ma conciso.</span>
              </div>
            </div>
            <input type="submit" id="aggiungi" name="aggiungi" class="btn btn-primary btn-large" value="Aggiungi"/>
            <label for="reset" class="sr-only">Azzera tutti i campi.</label>
            <input type="reset" class="btn btn-outline-primary" id="reset" name="reset" value="Azzera" />
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Rimuovi Ingredienti</h3>
      </div>
      <div class="panel-body">
        <table class="table table-condensed table-hover table-bordered" id="tableing">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Operazioni</th>
            </tr>
          </thead>
          <tbody id="tablebody">
            <?php
            $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
            if($conn->connect_errno) {
              $esito = "connerr";
            } else {
              $user = $_SESSION["login_user"];
              $userid = $user["IdUtente"];
              $sql = "SELECT * FROM Ingrediente ORDER BY Nome";
              $result = $conn->query($sql);
              while(($row = $result->fetch_array(MYSQLI_ASSOC)) != null) {
                echo "<tr>";
                echo "  <td>" . $row["Nome"] . "</td>";
                echo "  <td><a class='add-ing-link' href='" . $row['IdIngrediente'] . "' ><em id='" . $row['IdIngrediente'] . "' class='fa fa-trash-o' aria-hidden='true'></em>&nbsp;Elimina</a></td>";
                echo "</tr>";
              }
              $conn->close();
            }
             ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
