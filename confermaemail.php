<?
if(!isset($_GET["IdUtente"])) {
  $esito = "negativo";
} else {
  $sql = "UPDATE Utente SET ConfEmail = 1 WHERE IdUtente = ? LIMIT 1 ;";
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  $query = $conn->prepare($sql);
  $query->bind_param("s", $email);
  $email = mysql_real_escape_string($_GET["IdUtente"]);
  $query->execute();
  $result = $query->get_result();
  if(!$result) {
    $esito = "positivo";
    header("Refresh: 5;URL=login.php");
  } else {
    $esito = "negativo";
  }
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Registrazione Completata</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets\bootstrap-3.3.7-dist\css\bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Invio Email di conferma</h3>
          </div>
          <div class="panel-body">
            <?php
            if(isset($esito) && $esito == "positivo") {
              echo '<div class="alert alert-success">';
              echo '<p>La registrazione è stata completata con successo.</p><p>Ora ti sarà possibile ordinare presso il nostro servzio e riceverlo comodamente dove vuoi.</p><p>È necessario adesso effettuare il primo login.</p><p>La pagina si ricaricherà in 5 secondi, se ciò non avenisse, cliccare <a href="login.php" class="link">qui.</a></p>';
              echo '</div>';
            } else {
              echo '<div class="row">
                      <div class="col-sm-12">
                        <div class="alert alert-warning">';
              echo "La registrazione non è andata a buon fine. Ciò può essere dovuto ad un problema di connessione al nostro server per il completamento della registrazione o ad un problema con l'invio dei dati per email. Riprovare a cliccare sul link inviato nell'email di conferma oppure richiedere l'invio di un altro messaggio contenente il link. Per ultimo, richiedere l'intervento di un amministratore di sistema.";
              echo '    </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <a href="inviaConfEmail.php?IdUtente='.$email.' class="btn btn-warning">ReInvia</a>
                        <a href="contattaci.php" class="btn btn-default">Contattaci</a>
                      </div>
                    </div>
                  </div>';
            }
             ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
    Da non modificare ed importare in ogni progetto.
    Deve rimanere sempre alla fine della pagina.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets\bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
</body>
</html>
