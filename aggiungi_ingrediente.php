<?php
$return = array('result' => 'null');
if(!isset($_POST['celiaco'])) {
  $return['result'] = 'error';
  $return['coderror'] = 'posterr';
} else {
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error){
    $return['result'] = 'error';
    $return['coderror'] = 'connerr';
  } else {
    $sql = "INSERT INTO Ingrediente(Nome) VALUES (?)";
    $query = $conn->prepare($sql);
    $query->bind_param("s", $nome);
    $nome = mysql_real_escape_string($_POST["nome"]);
    $query->execute();
    $result = $query->get_result();
    if (!$result) {
      $lastid = $conn->insert_id;
      $sql = "SELECT * FROM Ingrediente WHERE IdIngrediente = $lastid LIMIT 1";
      $result=$conn->query($sql);
      if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $return['result'] = 'success';
        $return['IdIngrediente'] = $row['IdIngrediente'];
        $return['Nome'] = $row['Nome'];
      } else {
        $return['result'] = 'error';
        $return['coderror'] = 'dataerr';
      }
    } else {
      $return['result'] = 'error';
      $return['coderror'] = $query->errno;
    }
    $conn->close();
  }
}
echo json_encode($return);
?>
