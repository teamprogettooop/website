<?php
if(!isset($_POST["registrati"])) {
  header("Refresh: 0;URL=register.php?error=nopost");
} else {
	require_once("constants.php");
	session_start();
	$conn = new mysqli($db_address, $db_root, $db_pass, $db_name);
	$conn->set_charset("utf8");
	if($conn->connect_error){
		header("Refresh: 0,URL:register.php?error=connerr");
	} else {
		$sql = "insert into Utente(Nome, Cognome, Email, Telefono, Password, Celiaco, Vegetariano, Vegano, Sale) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$query = $conn->prepare($sql);
		$query->bind_param("sssssiiis", $name, $surname, $email, $telephone, $passwordhash, $celiaco, $vegetariano, $vegano, $random_salt);
		$name = $conn->real_escape_string($_POST["name"]);
		$surname = $conn->real_escape_string($_POST["surname"]);
		$email = $conn->real_escape_string($_POST["email"]);
		$telephone = $conn->real_escape_string($_POST["telephone"]);
		$password = $conn->real_escape_string($_POST["password"]);
		$conpassword = $conn->real_escape_string($_POST["conpassword"]);
		if($password != $conpassword) {
			header("Refresh: 0;URL=register.php?error=passne");
		} else {
			// Crea una chiave casuale
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			// Crea una password usando la chiave appena creata.
			$passwordhash = hash('sha512', $password.$random_salt);
			$show_patologies = get_site_config_by_name("SHOW_PATOLOGIES") == "true";
			if(isset($_POST["celiaco"]) && $_POST["celiaco"] === "on" && $show_patologies) {
				$celiaco = '1';
			} else {
				$celiaco = '0';
			}
			if(isset($_POST["vegano"]) && $_POST["vegano"] === "on" && $show_patologies) {
				$vegano = '1';
			} else {
				$vegano = '0';
			}
			if(isset($_POST["vegetariano"]) && $_POST["vegetariano"] === "on" && $show_patologies) {
				$vegetariano = '1';
			} else {
				$vegetariano = '0';
			}
			echo $name . "<br />";
			echo $surname . "<br />";
			echo $email . "<br />";
			echo $telephone . "<br />";
			echo $password . "<br />";
			echo $conpassword . "<br />";
			echo $random_salt . "<br />";
			echo $passwordhash . "<br />";
			$query->execute();
			$result = $query->get_result();
			if (!$result) {
				$lastid = $conn->insert_id;
				$query = $conn->prepare("insert into Potere(IdUtente, IdPermesso) values (?, 1)");
				$query->bind_param("s", $lastid);
				$query->execute();
				$result = $query->get_result();
				if(!$result) {
				$conn->close();
				header("Refresh: 3;URL=inviaConfEmail.php?IdUtente=".$lastid);
				exit;
				}
				$conn->close();
				header("Refresh: 0;URL=register.php?error=dataerr");
				exit;
			}
			header("Refresh: 0;URL=register.php?error=".$query->errno);
		}
	}
}
?>
