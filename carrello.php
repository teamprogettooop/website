<!DOCTYPE html>
<html lang="it">
<head>
  <title>Carrello</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" /><!--
      Questo foglio di stile e lo script sono il cuore pulsante di Bootstrap.
    --><link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.css" media="screen" />
    <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <!--
  carica l'header
  --><?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <!--
        carica il pannello di controllo.
        -->
        <?php
          if($user["Titolo"] != "Amministratore") {
            require "assets/filepart/pannello_controllo_part.php";
          } else {
            require "assets/filepart/pannello_controllo_admin_part.php";
          } 
        ?>
      </div>
      <div class="col-sm-9">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Elenco prodotti nel carrello</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                <table style="height: 100px;">
                  <col width="20%" />
                  <col width="80%" />
                  <tbody>
                    <?php
                      //verifica se un utente ha effettuato il login.
                      if(isset($_SESSION["login_user"])) {
                        //memorizza i dati dell'utente
                        $u = $_SESSION["login_user"];

                        //paraetri per accedere al DB.
                        $host = "localhost";
                        $user = "root";
                        $pass = "";
                        $database = "my_tentonisanzio";

                        $tot = 0;

                        //si connette al DB.
                        $conn = new mysqli($host, $user, $pass, $database);
                        $conn->set_charset("utf8");
                        if($conn->connect_error) {
                            die("Connessione fallita: " . $conn->connect_error);
                        }
                        //ricava la lista di prodotti presenti nel carrello di un utente.
                        $sql = "SELECT Prodotto.Nome as Prodotto, Carrello.Quantita, Prodotto.Prezzo, Prodotto.Link from (Carrello join Utente on Carrello.IdUtente = Utente.IdUtente) join Prodotto on Carrello.IdProdotto = Prodotto.IdProdotto where Utente.IdUtente = '" . $u["IdUtente"] . "'";
                        $result = $conn->query($sql);

                        //per ogni prodotto costruisce una riga di tabella con i suoi dati.
                        while($dati = $result->fetch_array()) {
                          echo '<tr>
                            <td rowspan="4" class="col-sm-2 align-middle">
                              <img src=' . $dati["Link"] . ' class="img-responsive" id=' . $dati["Link"] . 'alt=' . $dati["Link"] . '>
                            </td>
                            <td class="col-sm-10 align-middle">
                              <h4> ' . $dati["Prodotto"] . '</h4>
                            </td>
                          </tr>
                          <tr>
                            <td class="col-sm-10 align-middle">
                              <h5>Quantità: ' . $dati["Quantita"] . '</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="col-sm-10 align-middle">
                              <h5>Prezzo: ' . $dati["Prezzo"] . '€</h5>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="3" align="right" class="col-sm-10 align-middle">
                              <form>
                                <input type="image" src="assets/img/delete.png" onclick="rimuovi(\'' . $dati["Prodotto"] . '\')" alt="rimuovi dal carrello" width="40" height="40">
                              </form>
                            </td>
                          </tr>';
                          $tot = $tot + $dati["Prezzo"] * $dati["Quantita"];
                        }
                        $conn->close();

                        //se il carrello non è vuoto mostra il totale della spesa, sennò avvisa l'utente di inserire dei prodotti.
                        if($tot > 0) {
                        echo '
                          <tr>
                            <td colspan="3" align="right"><h4>Totale : ' . $tot . '€</h4></td>
                          </tr>';
                        } else {
                          echo '
                          <tr>
                            <td colspan="3"><h2>Carrello vuoto! Inserisci dei prodotti prima di procedere.</h2></td>
                          </tr>';
                        }
                    }?>
                  </tbody>
                </table>
                <script>
                  //funzione associata al bottone che elimina un determinato prodotto dal carrello.
                  function rimuovi(prod) {
                    $.ajax({
                      type: 'POST',
                      url: 'carrello.php',
                      data: {'prod': prod},
                    });
                    <?
                      $pr = $_POST['prod'];
                      $us = $u["Nome"];

                      $conn = new mysqli($host, $user, $pass, $database);
                      if($conn->connect_error) {
                          die("Connessione fallita: " . $conn->connect_error);
                      }

                      $sql = "DELETE from Carrello where Carrello.IdProdotto = (SELECT IdProdotto from Prodotto where Nome = '" . $pr . "') and Carrello.IdUtente = '" . $u["IdUtente"] ."'";
                      $conn->query($sql);
                      $conn->close();
                    ?>
                  }
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
          <div class="col-sm-2 pull-right">
            <a href="ultimopasso.php"  class="btn btn-primary" <?if ($tot == '0'){ ?> disabled <? } ?>>Procedi al pagamento</a>
          </div>
        </div>
      </div>
  </div>
  <!--
  carica il footer.
  --><?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
