<?php
$return = array('result' => 'null');
if(!isset($_GET['id'])) {
  $return['result'] = 'error';
  $return['coderror'] = 'geterr';
} else {
  $return['id'] = $_GET['id'];
  $return['postid'] = $_POST['id'];
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error){
    $return['result'] = 'error';
    $return['coderror'] = 'connerr';
  } else {
    $sql = "DELETE FROM Ingrediente WHERE IdIngrediente = ?";
    $query = $conn->prepare($sql);
    $query->bind_param("s", $IdIng);
    $IdIng = mysql_real_escape_string($_GET["id"]);
    $query->execute();
    $result = $query->get_result();
    if (!$result) {
      $sql = "DELETE FROM Preparato WHERE IdIngrediente = ?";
      $query = $conn->prepare($sql);
      $query->bind_param("s", $IdIng);
      $IdIng = mysql_real_escape_string($_GET["id"]);
      $query->execute();
      $result = $query->get_result();
      $return['result'] = 'success';
    } else {
      $return['result'] = 'error';
      $return['coderror'] = $query->errno;
    }
    $conn->close();
  }
}
echo json_encode($return);
?>
