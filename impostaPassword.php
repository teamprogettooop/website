<?php
if(!isset($_GET["codicerecupero"]) || !isset($_GET["mail"])) {
  $esito = "negativo";
} else {
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_errno) {
    $esito = "negativo";
  } else {
    $sql = "SELECT IdUtente, Nome FROM Utente WHERE Password = ? AND Email = ? LIMIT 1";
    $query = $conn->prepare($sql);
    $query->bind_param('ss', $passwd, $mail);
    $passwd = mysql_real_escape_string($_GET["codicerecupero"]);
    $mail = mysql_real_escape_string($_GET["mail"]);
    $query->execute();
    $result = $query->get_result();
    if($result->num_rows > 0) {
      $user = $result->fetch_array(MYSQLI_ASSOC);
      $conn->close();
      $userid = $user["IdUtente"];
      $userna = $user["Nome"];
      $esito = "positivo";
    } else {
      $esito = "negativo";
    }
  }
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Recupero Password</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Recupero password</h3>
          </div>
          <div class="panel-body">
          <?php
          if(isset($esito) && $esito == "positivo") {
            echo 'Gentile ' . $userna . ', digitare di seguito la nuova password. Successivamente, cliccare sul bottone Conferma per ultimare la procedura di cambio della password.';
            echo '<form action="cambiaPassword.php?userid='. $userid . '" method="post" autocomplete="true">';
            echo '  <fieldset>';
            echo '    <div class="form-group row">';
            echo '      <label for="password" class="col-form-label col-sm-3">';
            echo '        Password';
            echo '      </label>';
            echo '      <div class="col-sm-9">';
            echo '        <input type="password" class="form-control" name="password" id="password" placeholder="Password" aria-described="passwordTip" required/>';
            echo '        <span id="passwordTip" class="help-block">Non condivideremo la sua password con nessuno.</span>';
            echo '      </div>';
            echo '    </div>';
            echo '    <div class="form-group row">';
            echo '      <label for="conpassword" class="col-form-label col-sm-3">';
            echo '        Conferma Password';
            echo '      </label>';
            echo '      <div class="col-sm-9">';
            echo '        <input type="password" class="form-control" name="conpassword" id="conpassword" placeholder="Conferma Password" aria-described="conpasswordTip" required/>';
            echo '        <span id="conpasswordTip" class="help-block">Per maggiore sicurezza chiediamo di confermare la password.</span>';
            echo '      </div>';
            echo '    </div>';
            echo '  </fieldset>';
            echo '  <input type="submit" class="btn btn-primary btn-large" value="Invia" id="recupera" name="recupera" />';
            echo '</form>';
          } else {
            echo '<div class="alert alert-warning">';
            echo '  Deve esserci stato un problema con il reindirizzamento da una pagina del nostro sito o nel recupero dei dati dal database, vi preghiamo di ripetere la procedura. se il problema dovesse persistere, vi preghiamo di contattare un amministratore di sistema tramite <a href="contattaci.php" class="alert-link">questo link.</a>';
            echo '</div>';
          }
           ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
    Da non modificare ed importare in ogni progetto.
    Deve rimanere sempre alla fine della pagina.
  -->
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>
