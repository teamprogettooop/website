<?php require_once("constants.php"); ?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title><?php echo($project_name); ?></title><!--
		Vari metadati
	--><meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" /><!--
		Ogni altro foglio di stile deve trovarsi qua sotto.
		Compreso il foglio delle icone.
	-->
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
	<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
	<?php require "assets/filepart/header.php"; ?><!--
		Foto di sfondo e barra di ricerca pronta all'uso.
	--><div class="container">
		<div class="row align-items-center">
			<div class="col-sm-12 text-center jumbotron header"></div>
		</div>
	</div><!--
		Inizio della sezione di presentazione del servizio.
	--><div class="container">
		<div class="row">
			<div class="col">
				<h3><?php echo($project_name); ?></h3>
				<p><strong>Ordina online.</strong><br />Ordina la spesa online e vienila a ritirare a casa nostra oppure mettiamoci d'accordo per fartela recapitare a casa!</p>
				<div class="row">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
					<a href="prodotti.php" class="btn btn-primary btn-lg btn-block"><em class="fa fa-th-list fa-fw" aria-hidden="true"></em>&nbsp;Vai ai Prodotti</a>
				</div>
				<div class="col-sm-4">
				</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h3 class="text-center text-danger">Zona rossa</h3>
				<p class="lead text-center">Essendo il nostro punto vendita principale all'interno della zona rossa, non vi possiamo più accedere. Per questo stiamo cercando, fino a quando l'emergenza non passerà, di continuare la nostra attività anche qui a casa nostra e nella zona di Cesena. Per maggiori informazioni, clicca qui.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h3 class="text-center">Come Funziona</h3>
				<p class="lead text-center">Registrati o esegui il login, segui questi tre passaggi e goditi il tuo pasto comodamente a casa! Siamo anche su <a href="https://www.facebook.com/Frutta-Verdura-Tentoni-Sanzio-481175092388275/">Facebook</a>.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 text-center">
				<em class="fa fa-search fa-4x" aria-hidden="true"></em>
				<h4>Aggiungi al Carrello</h4>
				<p>Cerca tra tutti i prodotti disponibili nel catalogo dei prodotti aggiornato. Vendiamo frutta e verdura fresche, acquistate quotidianamente, anche da produttori locali.</p>
			</div>
			<div class="col-sm-4 text-center">
				<em class="fa fa-money fa-4x" aria-hidden="true"></em>
				<h4>Effettua il pagamento</h4>
				<p>Paga in contanti o tramite carta di credito direttamente al ritiro della merce o alla consegna.</p>
			</div>
			<div class="col-sm-4 text-center">
				<em class="fa fa-gift fa-4x" aria-hidden="true"></em>
				<h4>Ritiralo in sede</h4>
				<p>Vieni a ritirarlo direttamente a casa nostra oppure mettiti d'accordo per un eventuale consegna a domicilio. Consegnamo in tutta la zona di Cesena città, Calisese, Gambettola e dintorni.</p>
			</div>
		</div>
	</div>
	<?php require "assets/filepart/footer.php"; ?>
	<!--
		Questa parte serve per poter eseguire gli script interni a Bootstrap.
		Da non modificare ed importare in ogni progetto.
		Deve rimanere sempre alla fine della pagina.
	--><script src="http://code.jquery.com/jquery.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>
