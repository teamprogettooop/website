<?php
	$return = array("destinazione" => $_POST["dest"]);
    session_start();
		//verifica se un utente ha effettuato il login.
    if(isset($_SESSION["login_user"])) {
			//memorizza i dati dell'utente.
      $u = $_SESSION["login_user"];

			//parametri per la connessione al DB.
      $host = "localhost";
      $user = "root";
      $pass = "";
      $database = "my_tentonisanzio";

			//connessione al DB.
      $conn = new mysqli($host, $user, $pass, $database);
	  $conn->set_charset("utf8");
      if($conn->connect_error) {
        echo "Connessione fallita: " . $conn->connect_error;
      }

			//ricava i prodotti presenti nel carrello di un utente.
		  $sql = "SELECT * from (Carrello join Prodotto on Carrello.IdProdotto = Prodotto.IdProdotto) where IdUtente=" . $u["IdUtente"];
		  $ris = $conn->query($sql);

		$idcarr = 0;
		$i = 0;
		  
		  $return["while"] = "Non entrato";
		  $return["righe"] = $ris->num_rows;
		  
      while($dati = $ris->fetch_array()) {
			  $return["while"] = "Entrato";

				//utilizza il primo idCarrello come id dello storico per tutti i prodotti del carrello di un utente.
	      if($idcarr == 0) {
	        $idcarr = $dati["IdCarrello"];
			$return["carrello"]=$dati["IdCarrello"];
	      }

				//inserisce nello storico i prodotti presenti nel carrello di un utente.
				$sqlins = "INSERT INTO Storico (IdOrdine, IdUtente, IdProdotto, Quantita, ImportoUnitario, DataOrdine, Consegna, StatoConsegna) VALUES (" .
				$idcarr . ", " . $dati["IdUtente"] . ", " . $dati["IdProdotto"] . ", " . $dati["Quantita"] . ", " . $dati["Prezzo"] . ", curdate(), ?, 1)";
				$query=$conn->prepare($sqlins);
				$query->bind_param('s',$dest);
				$dest=mysql_real_escape_string($_POST["dest"]);
				$query->execute();
				$result = $query->get_result();
				$return[$i++] = $sqlins . " Consegna: " . $dest;
      }
	  
	  $sql="DELETE FROM Carrello WHERE IdUtente=". $u["IdUtente"];
	  $result=$conn->query($sql);
	  
      $conn->close();
    }
	echo json_encode($return);
?>
