<?php
// Company informations.
$companyname = "Tentoni Sanzio & C. S.N.C.";
$adminmail = "daniele.tentoni.1996@gmail.com";

// Project info.
$project_name = "Tentoni Sanzio & C.";
$project_mail = "tentonisanzio@altervista.org";
$project_base_url = "www.tentonisanzio.altervista.org";

// Database constants.
$db_address = "localhost";
$db_name = "my_tentonisanzio";
$db_root = "root";
$db_pass = "";

/*
 * Set if this project can show patologies or no.
 */
function get_site_config_by_name($config_name) {
	// Declare global variables.
	global $db_address, $db_root, $db_pass, $db_name;

	// Connect to the single db.
	$conn = new mysqli($db_address, $db_root, $db_pass, $db_name);
	$conn->set_charset("utf8");
	if($conn->connect_errno) {
		return false;
	} else {
		$query = "SELECT SetValue FROM site_config WHERE Name = ?";
		$stmt = $conn->prepare($query);
		$stmt->bind_param("s", $config_name);
		$stmt->execute(); // Esegue la query appena creata.
		$result = $stmt->get_result();
		if($result->num_rows == 1) {
			// Return the setvalue of the config when found.
			$fetch = $result->fetch_array(MYSQLI_ASSOC);
			return $fetch["SetValue"];
		} else {
			// Return false if no config is found.
			return false;
		}
	}
}
?>