<?php
if(isset($_GET["id"])) {
  $host="localhost";
  $user="root";
  $pass="";
  $database="my_tentonisanzio";
  $conn = new mysqli($host,$user,$pass,$database);
  $conn->set_charset("utf8");
  if($conn->connect_error){
    die("Connessione fallita:".$conn->connect_error);
  }

  $sql = "SELECT Prodotto.Descrizione from Prodotto where IdProdotto = " . $_GET["id"] . " limit 1" ;
  $result=$conn->query($sql);

  while($row= $result->fetch_assoc()){
    $descrizione = $row["Descrizione"];
  }
  $conn->close();
}
if(isset($_POST["submit"])) {
  $host="localhost";
  $user="root";
  $pass="";
  $database="my_tentonisanzio";
  $conn=new mysqli($host,$user,$pass,$database);
  if($conn->connect_error){
    die("Connessione fallita:".$conn->connect_error);
  }

  $sql = "UPDATE Prodotto SET Prodotto.Descrizione = '" . $_POST["descrizione"] . "' where IdProdotto = " . $_GET["id"] . " limit 1" ;
  $result=$conn->query($sql);
  if(!$result) {
    $esito = "positivo";
  } else {
    $esito = $result->error;
  }
  $conn->close();
}
 ?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Mdofica Prodotti</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php
  require "assets/filepart/header.php";
  ?>
  <div class="container">
    <div class="row">
      <form name="ricarica" method="get" action="">
        <div class="col-sm-6">
          <select class="form-control" name="id">
            <?php
          	$host="localhost";
          	$user="root";
          	$pass="";
          	$database="my_tentonisanzio";
            $conn = new mysqli($host,$user,$pass,$database);
            $conn->set_charset("utf8");
          	if($conn->connect_error){
          		die("Connessione fallita:".$conn->connect_error);
          	}

            $sql = "SELECT Prodotto.IdProdotto, Prodotto.Nome from Prodotto" ;
          	$result=$conn->query($sql);

          	while($row= $result->fetch_assoc()){
          		echo "<option value='" . $row['IdProdotto'] . "'>" . $row['Nome'] . "</option>";
          	}
          	$conn->close();
             ?>
          </select>
        </div>
        <div class="col-sm-6">
          <input type="submit" class="btn btn-default btn-large" />
        </div>
      </form>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <form action="" method="post">
          <fieldset>
            <div class="form-group row">
              <textarea name="descrizione" id="descrizione" placeholder="Descrizione" class="form-control"><?php
                  if(isset($descrizione)) {
                    echo $descrizione;
                  }
                ?></textarea>
            </div>
            <input type="submit" id="submit" name="submit" class="btn btn-primary btn-large"/>
          </fieldset>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <?php
        if(isset($esito)) {
          echo $esito;
        }
         ?>
      </div>
    </div>
  </div>
  <?php
  require "assets/filepart/footer.php";
  ?>
<!--
  Questa parte serve per poter eseguire gli script interni a Bootstrap.
  Da non modificare ed importare in ogni progetto.
  Deve rimanere sempre alla fine della pagina.
--><script src="http://code.jquery.com/jquery.js"></script>
<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>
