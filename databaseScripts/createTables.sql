create database if not exists my_tentonisanzio character set utf16;

use my_tentonisanzio;

create table if not exists site_config (
    Id int AUTO_INCREMENT UNIQUE,
    Name varchar(30) not null,
    SetValue varchar(30) not null,
    Description varchar(100),
    constraint PK_Site_Config primary key (Id)
);

create table if not exists Utente (
  IdUtente int auto_increment not null unique,
  Nome varchar(30) not null,
  Cognome varchar(30) not null,
  Email varchar(30) not null unique,
  /* Amico boolean not null unique */
  Telefono varchar(10) not null,
  Password varchar(129) not null,
  Celiaco boolean default false,
  Vegetariano boolean default false,
  Vegano boolean default false,
  /* RegisterDate date not null */
  ConfEmail boolean default false,
  Sale varchar(129) not null,
  constraint PK_Utente primary key (IdUtente)
);

create table if not exists Login (
  IdUtente int not null,
  Data varchar(30) not null,
  constraint PK_Login primary key (IdUtente)
);

create table if not exists Permesso (
  IdPermesso int auto_increment not null unique,
  Titolo varchar(30) not null unique,
  CanAddIng boolean default false,
  CanAddUsr boolean default false,
  CanAddPrd boolean default false,
  CanRmvIng boolean default false,
  CanRmvUsr boolean default false,
  CanRmvPrd boolean default false,
  CanChaOrd boolean default false,
  constraint PK_Permesso primary key (IdPermesso)
);

create table if not exists Potere (
  IdUtente int not null,
  IdPermesso int not null,
  constraint PK_Potere primary key (IdUtente, IdPermesso),
  constraint FK_PotereUtente foreign key (IdUtente) references Utente(IdUtente) on delete cascade, -- I'm not sure about this.
  constraint FK_PoterePermesso foreign key (IdPermesso) references Permesso(IdPermesso)
);

create table if not exists Categoria (
  IdCategoria int auto_increment not null unique,
  Nome varchar(50) not null unique,
  constraint PK_Categoria primary key (IdCategoria)
);

create table if not exists Prodotto (
  IdProdotto int auto_increment not null unique,
  Nome varchar(100) not null unique,
  Prezzo double not null,
  Descrizione varchar(5000) not null,
  Categoria int not null,
  Link varchar(100) not null,
  LinkBig varchar(100) not null,
  Abilitato boolean not null default true,
  Vegetariano boolean not null default false,
  Vegano boolean not null default false,
  Celiaco boolean not null default false,
  constraint PK_Prodotto primary key (IdProdotto),
  constraint FK_Categoria foreign key (Categoria) references Categoria(IdCategoria)
);

create table if not exists Ingrediente (
  IdIngrediente int auto_increment not null unique,
  Nome varchar(30) not null unique,
  constraint PK_Ingrediente primary key (IdIngrediente)
);

create table if not exists Preparato (
  IdProdotto int not null,
  IdIngrediente int not null,
  constraint PK_Preparato primary key (IdProdotto, IdIngrediente),
  constraint FK_Prodotto foreign key (IdProdotto) references Prodotto(IdProdotto),
  constraint FK_Ingrediente foreign key (IdIngrediente) references Ingrediente(IdIngrediente)
);

create table if not exists Carrello (
  IdCarrello int not null auto_increment unique,
  IdUtente int not null,
  IdProdotto int not null,
  Quantita int not null default 1,
  constraint PK_Carrello primary key (IdCarrello),
  constraint FK_Utente foreign key (IdUtente) references Utente(IdUtente),
  constraint FK_Prodotto foreign key (IdProdotto) references Prodotto(IdProdotto)
);

create table if not exists StatoConsegna (
  IdStato int auto_increment not null unique,
  Nome varchar(50) not null,
  constraint PK_StatoConsegna primary key (IdStato)
);

create table if not exists Storico (
  IdStorico int auto_increment not null unique,
  IdOrdine int not null,
  IdUtente int not null,
  IdProdotto int not null,
  Quantita int not null default 1,
  ImportoUnitario double not null,
  DataOrdine int not null,
  StatoConsegna int not null,
  Consegna varchar(100) not null,
  constraint PK_Storico primary key (IdStorico),
  constraint FK_Utente foreign key (IdUtente) references Utente(IdUtente),
  constraint FK_Prodotto foreign key (IdProdotto) references Prodotto(IdProdotto),
  constraint FK_StatoConsegna foreign key (StatoConsegna) references StatoConsegna(IdStato)
);

-- Trigger to delete power of a user before his delete.
DELIMITER $$
CREATE TRIGGER delete_user_delete_power
    BEFORE DELETE
    ON Utente FOR EACH ROW
BEGIN
    DELETE FROM Potere WHERE UserId = OLD.IdUtente;
END$$
DELIMITER ;