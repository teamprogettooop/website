INSERT INTO `Utente` (`IdUtente`, `Nome`, `Cognome`, `Email`, `Telefono`, `Password`, `Celiaco`, `Vegetariano`, `Vegano`, `ConfEmail`) VALUES
(1, 'Daniele', 'Tentoni', 'daniele.tentoni.1996@gmail.com', '3487384621', 'admin', 0, 0, 0, 0),
(2, 'Mio', 'Fratello', 'noreplay@famiglia.com', '1234567890', 'user', 0, 0, 0, 1),
(3, 'Nicolò', 'Pracucci', 'npracucci@gmail.com', '3493342340', 'apenzapina', 0, 0, 0, 1),
(4, 'Luca', 'Mondaini', 'luca@mondaini.com', '1234567890', 'monda', 1, 1, 1, 0),
(5, 'dada', 'dada', 'a@b.com', '1234', '1234', 0, 0, 0, 0),
(6, 'fafa', 'fafa', 'b@a.com', '1234', '1234', 1, 1, 1, 0);

insert into Permesso(IdPermesso, Titolo, CanAddIng, CanAddUsr, CanAddPrd, CanRmvIng, CanRmvUsr, CanRmvPrd, CanChaOrd) values
(1, "Utente", 0, 0, 0, 0, 0, 0, 0),
(2, "Amministratore", 1, 1, 1, 1, 1, 1, 0),
(3, "Operatore", 0, 0, 0, 0, 0, 0, 1);

insert into Potere(IdUtente, IdPermesso) values
  (1, 2),
  (2, 1);

insert into Categoria(IdCategoria, Nome) values
  (1, "Antipasto"),
  (2, "Primo"),
  (3, "Secondo"),
  (4, "Contorno"),
  (5, "Dolce"),
  (6, "Frutta");

insert into Prodotto(IdProdotto, Nome, Prezzo, Descrizione, Categoria, Link, Abilitato) values
  (1, "Mezze Penne al Pomodoro e Basilico, 80gr", 3.6, "Pasta di grano duro (Mezze Penne) al sugo di pomodoro. Un primo piatto semplice e appettitoso. Per chi vuole coniugare leggerezza con un palato delicato. Preparato solamente con basilico nostrano e passato di pomodoro IGP.", 2, "assets/imgproducts/mezze-penne-pomodoro-basilico.png", true),
  (2, "Cotoletta alla Milanese, 100gr", 4.5, "Petto di pollo impanato e cotto in forno. Un secondo piatto assolutamente sostanzioso.", 3, "assets/imgproducts/cotoletta-milanese.png", true),
  (3, "Grissino", 1, "Antipasto veloce e molto leggero.", 1, "http://www.gruppopregis.it/s_files/prodotti/DIS/24050364_20131024170305.PNG", 1);

insert into Ingrediente(IdIngrediente, Nome, Vegetariano, Vegano, Celiaco) values
  (1, "Mezze Penne", 1, 0, 0),
  (2, "Sugo di Pomodoro", 1, 0, 1),
  (3, "Basilico", 1, 1, 1),
  (4, "Petto di Pollo", 0, 0, 1),
  (5, "Pan grattato", 1, 1, 0);

insert into Preparato(IdProdotto, IdIngrediente) values
  (1, 1),
  (1, 2),
  (1, 3),
  (2, 4),
  (2, 5);

insert into site_config(Name, SetValue, Description) values 
("SHOW_PATOLOGIES", "false", "Mostra oppure no la possibilità di impostare le patologie alimentari degli utenti con tutti i relativi filtri in tutte le relative aree.");

SELECT Prodotto.Nome, Ingrediente.Nome
FROM (Prodotto JOIN
      (Preparato JOIN Ingrediente
       ON Preparato.IdIngrediente = Ingrediente.IdIngrediente)
      ON Prodotto.IdProdotto = Preparato.IdProdotto)
WHERE Prodotto.IdProdotto = (SELECT MAX(Prodotto.IdProdotto) FROM Prodotto)
