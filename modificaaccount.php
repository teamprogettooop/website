<?php
if(!isset($_POST["modifica"])) {
  header("Refresh: 0;URL=account.php?coderror=nopost");
} else {
  session_start();
  $user = $_SESSION["login_user"];
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error){
    header("Refresh: 0;URL=account.php?coderror=connerr");
  } else {
    $sql = "UPDATE Utente SET Nome=?, Cognome=?, Email=?, Telefono=?, Celiaco=?, Vegetariano=?, Vegano=? WHERE IdUtente = ?";
    $query = $conn->prepare($sql);
    $query->bind_param("ssssiiii", $name, $surname, $email, $telephone, $celiaco, $vegetariano, $vegano, $idUtente);
    $idUtente = $user["IdUtente"];
    if(isset($_POST["name"])) {
      $name = mysql_real_escape_string($_POST["name"]);
    } else {
      $name = $user["Nome"];
    }
    if(isset($_POST["surname"])) {
      $surname = mysql_real_escape_string($_POST["surname"]);
    } else {
      $surname = $user["Cognome"];
    }
    if(isset($_POST["email"])) {
      $email = mysql_real_escape_string($_POST["email"]);
    } else {
      $email = $user["Email"];
    }
    if(isset($_POST["telephone"])) {
      $telephone = mysql_real_escape_string($_POST["telephone"]);
    } else {
      $telephone = $user["Telefono"];
    }
    if(isset($_POST["celiaco"])) {
      if($_POST["celiaco"] == "on") {
        $celiaco = '1';
      } else {
        $celicaco = '0';
      }
    } else {
      $celicaco = '0';
    }
    if(isset($_POST["vegano"])) {
      if($_POST["vegano"] == "on") {
        $vegano = '1';
      } else {
        $vegano = '0';
      }
    } else {
      $vegano = '0';
    }
    if(isset($_POST["vegetariano"])) {
      if($_POST["vegetariano"] == "on") {
        $vegetariano = '1';
      } else {
        $vegetariano = '0';
      }
    } else {
      $vegetariano = '0';
    }
    $query->execute();
    $result = $query->get_result();
    if (!$result) {
      if(isset($_POST["password"]) && $_POST["password"] != "" && isset($_POST["conpassword"])) {
        $password = mysql_real_escape_string($_POST["password"]);
        $conpassword = mysql_real_escape_string($_POST["conpassword"]);
        if($password != $conpassword) {
          header("Refresh: 0;URL=account.php?coderror=passne");
        } else {
          $query = $conn->prepare("UPDATE Utente SET Password = ? WHERE IdUtente = ?");
          if($conn->connect_error){
            header("Refresh: 0;URL=account.php?coderror=connerr");
          } else {
            $query->bind_param("si", $passwordmd5, $idUtente);
            $passwordmd5 = md5($password);
            $query->execute();
            $result = $query->get_result();
            if(!$result) {
              $conn->close();
              header("Refresh: 0;URL=account.php?coderror=ok&elem=Account");
            } else {
              $conn->close();
              header("Refresh: 0;URL=account.php?coderror=chnpwd");
            }
          }
        }
      } else {
        $conn->close();
        header("Refresh: 0;URL=account.php?coderror=ok&elem=Account");
      }
    } else {
      $conn->close();
      header("Refresh: 0;URL=register.php?coderror=".$query->errno);
    }
  }
}
?>
