<?php
	//Se la chiamata è ajax allora continua.
	if(is_ajax()){
			//Controlla il link cliccato in prodotti.php
			$action=$_POST["action"];
			switch($action){
				case "antipasti":
					prodotti("Antipasto");
				break;
				case "primi":
					prodotti("Primo");
				break;
				case "secondi":
					prodotti("Secondo");
				break;
				case "contorni":
					prodotti("Contorno");
				break;
				case "dolce":
					prodotti("Dolce");
				break;
				case "frutta":
					prodotti("Frutta");
				break;
			}
	}

	//Funzione che serve per controllare se la chiamata è ajax o no.
	function is_ajax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}

	//Funzione che carica i prodotto da visualizzare nel sito.
	function prodotti($categoria){
	$celiaco=$_POST["celiaco"];
	$vegetariano=$_POST["vegetariano"];
	$vegano=$_POST["vegano"];
	$fin = "";
	
	//Info database.
	$host="localhost";
	$user="root";
	$pass="";
	$database="my_tentonisanzio";

	//Connessione al database.
	$conn=new mysqli($host, $user, $pass, $database);
	//Codifica caratteri nel database.
	$conn->set_charset("utf8");
	//In caso di errore.
	if($conn->connect_error){
		die("Connessione fallita:".$conn->connect_error);
	}

	//Varie possibili soluzioni.
	if($celiaco=="0" && $vegetariano=="0" && $vegano=="0"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."'";
	}
	if($celiaco=="0" && $vegetariano=="0" && $vegano=="1"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Vegano='1'";
	}
	if($celiaco=="0" && $vegetariano=="1" && $vegano=="0"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Vegetariano='1'";
	}
	if($celiaco=="0" && $vegetariano=="1" && $vegano=="1"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Vegano='1' and Prodotto.Vegetariano='1'";
	}
	if($celiaco=="1" && $vegetariano=="0" && $vegano=="0"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Celiaco='1'";
	}
	if($celiaco=="1" && $vegetariano=="0" && $vegano=="1"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Vegano='1' and Prodotto.Celiaco='1'";
	}
	if($celiaco=="1" && $vegetariano=="1" && $vegano=="0"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Celiaco='1' and Prodotto.Vegetariano='1'";
	}
	if($celiaco=="1" && $vegetariano=="1" && $vegano=="1"){
		$sql="SELECT Prodotto.Nome,Prodotto.Link from (Prodotto join Categoria on Prodotto.Categoria=Categoria.IdCategoria)".
		"where Categoria.Nome = '".$categoria."' and Prodotto.Celiaco='1' and Prodotto.Vegetariano='1' and Prodotto.Vegano='1'";
	}
	
	//Salvataggio risultato query.
	$result=$conn->query($sql);

	//Creazione stringa di ritorno.
	while($row= $result->fetch_assoc()){
		$fin = $fin . $row["Nome"] . '*' . $row["Link"] . '^';
	}

	$return["pasti"]=$fin;

	//Chisura connessione.
	$conn->close();

	//Ritorno.
	echo json_encode($return);
	}
?>
