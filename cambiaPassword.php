<?
if(!isset($_POST["password"]) || !isset($_GET["userid"])) {
  $esito = "negativo";
} else {
  if($_POST["password"] != $_POST["conpassword"]) {
    $esito = "password";
  }
  $sql = "UPDATE Utente SET Password = ? WHERE IdUtente = ? LIMIT 1";
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  $query = $conn->prepare($sql);
  $query->bind_param("ss", $password, $userid);
  $userid = mysql_real_escape_string($_GET["userid"]);
  $password = md5(mysql_real_escape_string($_POST["password"]));
  $query->execute();
  $result = $query->get_result();
  if(!$result) {
    $esito = "positivo";
    header("Refresh: 0;URL=login.php");
  } else {
    $esito = "negativo";
  }
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Recupero Password</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets\bootstrap-3.3.7-dist\css\bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Recupero Password</h3>
          </div>
          <div class="panel-body">
            <?php
            if(isset($esito)) {
              if($esito === "positivo") {
                echo '<div class="alert alert-success">
                        <p>Il recupero della password è stato completato con successo.</p>
                        <p>La pagina si ricaricherà in 5 secondi, se ciò non avenisse, cliccare <a href="login.php" class="link">qui.</a>
                      </div>';
              } else if ($esito === "password") {
                echo '<div class="row">
                        <div class="col-sm-12">
                          <div class="alert alert-warning">';
                echo "Sono state rilevate due password differenti. Vi preghiamo di tornare indietro e ritentare la procedura.";
                echo '    </div>
                        </div>
                      </div>
                    </div>';
              } else {
                echo '<div class="row">
                        <div class="col-sm-12">
                          <div class="alert alert-warning">';
                echo "Il recupero della password non è andato a buon fine. Ciò può essere dovuto ad un problema di connessione al nostro server per il completamento del recupero. Rieseguire l'operazione. Per ultimo, richiedere l'intervento di un amministratore di sistema.";
                echo '    </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <a href="passwordsmarrita.php?" class="btn btn-warning btn-large">Recupera Password</a>
                          <a href="contattaci.php" class="btn btn-default">Contattaci</a>
                        </div>
                      </div>
                    </div>';
              }
            }
             ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
    Da non modificare ed importare in ogni progetto.
    Deve rimanere sempre alla fine della pagina.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets\bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
</body>
</html>
