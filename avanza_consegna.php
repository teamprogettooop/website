<?php
$return = array('result' => 'null');
if(!isset($_GET['id'])) {
  $return['result'] = 'error';
  $return['coderror'] = 'geterr';
} else {
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_error){
    $return['result'] = 'error';
    $return['coderror'] = 'connerr';
  } else {
    $sql = "SELECT StatoConsegna FROM Storico WHERE IdOrdine = ? LIMIT 1";
    $query = $conn->prepare($sql);
    $query->bind_param("s", $IdIng);
    $IdIng = mysql_real_escape_string($_GET["id"]);
    $query->execute();
    $result = $query->get_result();
    if($result->num_rows > 0) {
      $consegna = $result->fetch_array(MYSQLI_ASSOC);
      if($consegna['StatoConsegna'] < 3 && $consegna['StatoConsegna'] > 0) {
        $sql = "UPDATE Storico SET StatoConsegna = StatoConsegna + 1 WHERE IdOrdine = ?";
        $query = $conn->prepare($sql);
        $query->bind_param("s", $IdIng);
        $IdIng = mysql_real_escape_string($_GET["id"]);
        $query->execute();
        $result = $query->get_result();
        if (!$result) {
          $return['result'] = 'success';
          $cons = $consegna['StatoConsegna'] + 1;
          $sql = "SELECT StatoConsegna.Nome as Nome, Utente.Email as Mail, Storico.IdOrdine as IdOrdine FROM (StatoConsegna JOIN (Storico JOIN Utente ON Storico.IdUtente = Utente.IdUtente) ON StatoConsegna.IdStato = Storico.StatoConsegna) WHERE Storico.IdOrdine = ? GROUP BY Storico.IdOrdine LIMIT 1";
          $query = $conn->prepare($sql);
          $query->bind_param("s", $IdIng);
          $IdIng = mysql_real_escape_string($_GET["id"]);
          $query->execute();
          $result = $query->get_result();
          if($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $return['consegna'] = $row["Nome"];
            /*
            * Invio della notifica.
            */
            $to = $row["Mail"];
            $email = "apenzapina@altervista.org";
            $oggetto = "Stato Consegna";
            $messaggio = "<html>";
            $messaggio.= "<head>";
            $messaggio.= "  <title>Avanzamento Ordine</title>";
            $messaggio.= "</head>";
            $messaggio.= "<body>";
            $messaggio.= "  <h1>Stiamo avanzando il tuo ordine!</h1>";
            $messaggio.= "  <p>Il tuo ordine attualmente è in stato di " . $row["Nome"] . "</p>";
            $messaggio.= "  <p>Se il servizio è di tuo gradimento, consiglialo ai tuoi amici. Se così non fosse contattaci tramite l'area apposito sul cliccando sul link sottostante, in questo modo sapremo come aiutarti.<br /><a href='apenzapina.altervista.org/contattaci.php'>apenzapina.altervista.org/contattaci.php</a>";
            $messaggio.= "</body>";
            $messaggio.= "</html>";
            $headers .= "From: APenzaPina <apenzapina@altervista.org>\r\n";
            $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
            $headers =  "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
            $headers .= "Content-Transfer-Encoding: 7bit\n\n";
            if( mail($to, $oggetto, $messaggio, $headers) ) {
              $return["esitoconsegna"] = "positivo";
            } else {
              $return["esitoconsegna"] = "negativo";
            }
          } else {
            $return['consegna'] = "Error";
          }
        } else {
          $return ['result'] = 'error';
          $return['coderror'] = 'updatefail';
        }
      } else {
        $return['result'] = 'error';
        $return['coderror'] = 'consegnafatta';
      }
    } else {
      $return['result'] = 'error';
      $return['coderror'] = $query->errno;
    }
    $conn->close();
  }
}
echo json_encode($return);
?>
