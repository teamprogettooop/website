<?php
  session_start();

  //verifica se un utente ha effettuato il login.
  if(isset($_SESSION["login_user"])) {
    //memorizza i dati dell'utente.
    $u = $_SESSION["login_user"];

    //parametri per la connessione al DB.
    $host = "localhost";
    $user = "root";
    $pass = "";
    $database = "my_tentonisanzio";

    //inizializza il messaggio di risposta.
    $msg["result"] = "";

    //connessione al DB.
    $conn = new mysqli($host, $user, $pass, $database);
    $conn->set_charset("utf8");
    if($conn->connect_error) {
        die("Connessione fallita: " . $conn->connect_error);
    }

    $qta = 0;

    //ricava la quantità di un prodotto nel carrello.
    $sql = "SELECT Carrello.Quantita from Carrello where Carrello.IdUtente = '" . $u["IdUtente"] ."' and Carrello.IdProdotto = '" . $_POST["prod"] . "'";
    $result = $conn->query($sql);

    while($dati = $result->fetch_array()) {
      $qta = $dati["Quantita"];
    }

    //se il prodotto non è presente nel carrello, lo inserisce; sennò ne aggiorna la quantità.
    if($qta === 0) {
      $sql = "INSERT INTO `Carrello`(`IdUtente`, `IdProdotto`, `Quantita`) values ('" . $u["IdUtente"] ."', '" . $_POST["prod"] . "', '" . $_POST["qta"] . "')";
    } else {
      $qta = $qta + $_POST["qta"];
      $sql = "UPDATE `Carrello` SET `Quantita`='" . $qta . "' where `IdUtente` = '" . $u["IdUtente"] ."' and `IdProdotto` = '" . $_POST["prod"] . "'";
    }

    $conn->query($sql);
    $conn->close();

    $msg["result"] = "Prodotto aggiunto correttamente!";
  } else {
    $msg["result"] = "Loggati per aggiungere i prodotti al tuo carrello!";
  }
  echo json_encode($msg);
?>
