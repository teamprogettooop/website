<?php
require_once("constants.php");
if(!isset($_GET["IdUtente"])) {

} else {
	$conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
	$conn->set_charset("utf8");
	if($conn->connect_errno) {
		echo "Fallita la connessione al database, contattare un amministratore di sistema.<br />Errore: ". $conn->connect_errno . "<br />Testo: " . $conn->connect_error;
		header("URL=login.php?error=connerr");
	}

	$sql = "SELECT Email FROM Utente WHERE IdUtente = ? LIMIT 1";
	$query = $conn->prepare($sql);
	$query->bind_param('s', $iduser);
	$iduser = $conn->real_escape_string($_GET["IdUtente"]);
	$query->execute();
	$result = $query->get_result();
	if($result->num_rows > 0) {
		$user = $result->fetch_array(MYSQLI_ASSOC);
		$conn->close();
		$to = $user["Email"];
		$email = $project_mail;
		$oggetto = "Conferma registrazione account";
		$messaggio = "<html>";
		$messaggio.= "<head>";
		$messaggio.= "  <title>Conferma Email</title>";
		$messaggio.= "</head>";
		$messaggio.= "<body>";
		$messaggio.= "  <h1>Benvenuto su {$project_name}!</h1>";
		$messaggio.= "  <p>Cliccando sul link sottostante completi la tua registrazione al sito e potrai iniziare ad usufruire del servizio.</p>";
		$messaggio.= "  <span><a href='http://{$project_base_url}/confermaemail.php?IdUtente=" . $iduser . "'>http://{$project_base_url}/confermaemail.php?IdUtente=" . $iduser . "</a></span>";
		$messaggio.= '<p>Se il link non funziona, incollalo nella barra degli indirizzi del tuo browser.</p>';
		$messaggio.= "  <p>Se non hai effettuato tu la registrazione presso il nostro sito, ti preghiamo di ignorare questa email ed eliminarla.</p>";
		$messaggio.= "  <p>In caso di bisogno, non rispondere direttamente a questa email, utilizzare direttamente il modulo <a href='{$project_base_url}/contattaci.php'>qui.</a>";
		$messaggio.= "</body>";
		$messaggio.= "</html>";
		$headers =  "MIME-Version: 1.0\r\n";
		$headers .= "From: {$project_name} <{$project_mail}>\r\n";
		$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
		$headers .= "Content-Transfer-Encoding: 7bit\n\n";
		if(mail($to, $oggetto, $messaggio, $headers)) {
			$esito = "positivo";
		} else {
			$esito = "negativo";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>Inviata Email di conferma</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	<link rel="stylesheet" type="text/css" href="assets\bootstrap-3.3.7-dist\css\bootstrap.min.css" media="screen" />
	<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
	<?php require "assets/filepart/header.php"; ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Invio Email di conferma</h3>
					</div>
					<div class="panel-body">
						<?php
						if(isset($esito) && $esito == "positivo") {
							echo '<div class="alert alert-success" role="alert">Email inviata a: ' . $to . ', speriamo che arrivi.</div>';
						} else {
							echo '<div class="row">
									<div class="col-sm-12">
										<div class="alert alert-warning" role="alert">';
							echo "Non sono riuscito a recuperare l'email o c'è stato un problema nella fase di invio del messaggio.<br/>Clicca sul bottone ReInvio sotto stante per richiedere l'invio di un ulteriore messaggio di conferma registrazione oppure clicca sul bottone Contattaci per contattarci direttamente e richiedere l'intervento di un amministratore. Cercheremo di rispondere appena possibile, nel più breve tempo.";
							echo '    </div>
									</div>
									</div>
									<div class="row">
									<div class="col-sm-12">
										<a href="inviaConfEmail.php?IdUtente=' . $iduser . '" class="btn btn-warning">ReInvia</a>
										<a href="contattaci.php" class="btn btn-default">Contattaci</a>
									</div>
									</div>
								</div>';
							// echo "Invio email a: " . $to . "<br /> con l'oggetto " . $oggetto . "<br /> con il messaggio " . $messaggio . "<br /> con gli headers " . $headers . "<br />";
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require "assets/filepart/footer.php"; ?>
	<!--
		Questa parte serve per poter eseguire gli script interni a Bootstrap.
		Da non modificare ed importare in ogni progetto.
		Deve rimanere sempre alla fine della pagina.
	--><script src="http://code.jquery.com/jquery.js"></script>
	<script src="assets\bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
</body>
</html>
