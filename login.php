<?php
if(isset($_SESSION["login-user"])) {
  header("Refresh: 0;URL=account.php");
}
 ?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Account</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php
  require "assets/filepart/header.php";
  if(isset($_COOKIE["Login"])) {
    $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
    $conn->set_charset("utf8");
    if($conn->connect_errno) {
      $cookie_error = "error";
    } else {
      $sql = "SELECT Utente.IdUtente, Nome, Cognome, Titolo, Telefono, Email, Celiaco, Vegetariano, Vegano, ConfEmail FROM Utente INNER JOIN (Potere INNER JOIN Permesso ON Potere.IdPermesso = Permesso.IdPermesso) ON Utente.IdUtente = Potere.IdUtente WHERE Utente.IdUtente = ? ";
      $query = $conn->prepare($sql);
      $login = mysql_real_escape_string($_COOKIE["Login"]);
      $query->bind_param('s', $login);
      $login = $_COOKIE["Login"];
      $query->execute();
      $result = $query->get_result();
      if($result->num_rows === 0) {
        $cookie_error = "error";
      }
      if($result->num_rows > 0) {
        $user = $result->fetch_array(MYSQLI_ASSOC);
        $_SESSION["login_user"] = $user;
        $conn->close();
      } else {
        $cookie_error = "error";
      }
    }
  }
  if(isset($_SESSION["login_user"])) {
    header("Refresh: 0;URL=account.php");
  } else {
    require "assets/filepart/login_part.php";
    require "assets/filepart/footer.php";
  }
  ?>
<!--
  Questa parte serve per poter eseguire gli script interni a Bootstrap.
  Da non modificare ed importare in ogni progetto.
  Deve rimanere sempre alla fine della pagina.
--><script src="http://code.jquery.com/jquery.js"></script>
<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>
