<!DOCTYPE html>
<html lang="it">
	<head>
		<title>Contattaci</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
	</head>
	<body>
	  <?php require "assets/filepart/header.php"; ?>
		<div class="container">
			<form name="formContattaci" action="contattaci.php" onsubmit="popup()" method="post" autocomplete="on">
				<fieldset>
					<legend>Contattaci</legend>
					<div class="form-group row align-items-left">
					  <label for="email" class="col-form-label col-sm-2">E-mail</label>
					  <div class="col-sm-12 text-left">
						<input type="email" class="form-control" name="email" id="email" placeholder="Indirizzo Email" required/>
						<small id="emailTip" class="form-text text-muted">Non condivideremo la sua email con nessuno.</small>
					  </div>
					</div>
					<div class= "form-group row align-items-left">
					  <label for="oggetto" class="col-form-label col-sm-2 text-left">Oggetto</label>
					  <div class="col-sm-12">
						<input type="text" name="oggetto" id="oggetto" class="form-control" placeholder="Oggetto" required/>
						<small id="oggettoTip" class="form-text text-muted">Oggetto del messaggio</small>
					  </div>
					</div>
					<div class= "form-group row align-items-left">
					  <label class="col-form-label col-sm-2 text-left">Messaggio</label>
					  <div class="col-sm-12">
						<label for="messaggio" class="sr-only">Messaggio</label>
						<textarea name="messaggio" id="messaggio" class="form-control areacontattaci" style="max-width: 100%" placeholder="Messaggio" required></textarea>
						<small id="messaggioTip" class="form-text text-muted">Messaggio da inviare</small>
					  </div>
					</div>
					<div class= "form-group row align-items-left form-check mb-2 mr-sm-2 mb-sm-0">
					<label for="checkbox" class="sr-only">Privacy</label>
					  <div class="col-sm-12 text-left">
						<input type="checkbox" class="form-check-input" id="checkbox" title="checkbox" name="privacy" value="privacy" required/> Ho letto la normativa sulla privacy e acconsento.
					  </div>
					</div>
					<div class= "form-group row align-items-left">
					  <div class="col-sm-12 text-left">
						<input type="submit" name="submit" id="submit" value="Invia" class="btn btn-primary"/>
					  </div>
					</div>
				</fieldset>
			</form>
		</div>
		<?php require "assets/filepart/footer.php";

			//Se la pagina è stata ricaricata mediante il bottone submit (Invia).
			if(isset($_POST["submit"])){

				//Manda un alert.
				echo "<script>";
				echo "alert(\"Grazie per averci contattato, ti risponderemo a breve\");";
				echo "</script>";

				//Manda email alla nostra mail di posta.
				$to="apenzapina@altervista.org";
				$email = $_POST["email"];
				$oggetto = $_POST["oggetto"];
				$mex = $_POST["messaggio"];
				$messaggio = "<html>";
				$messaggio.= "<head>";
				$messaggio.= "  <title>Segnalazione</title>";
				$messaggio.= "</head>";
				$messaggio.= "<body>";
				$messaggio.= "  <p>Segnalazione da " . $email . "</p>";
				$messaggio.= "  <p>". $mex . "</p>";
				$messaggio.= "</body>";
				$messaggio.= "</html>";
				$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
				$headers =  "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
				$headers .= "Content-Transfer-Encoding: 7bit\n\n";
				mail($to, $oggetto, $messaggio,$headers);
			}
		?>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>
