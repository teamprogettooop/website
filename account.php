<?php require_once("constants.php"); ?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>Account</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
	<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
	<?php require "assets/filepart/header.php"; ?><!--
		Pannello di controllo del login Utente
	--><?php
		/*
		* Logica di controllo dell'accesso avvenuto e dei permessi utente.
		* Se nessuno è loggato lo mando alla pagina di login e logon,
		* altrimenti lo indirizzo all'amministrazione del suo account.
		*/
		/*
		* Se non lo devo fare, allora procedo a visualizzare le relative pagine.
		*/
		if(isset($_SESSION["login_user"])) {
			$user = $_SESSION["login_user"];
			if($user["Titolo"] == "Amministratore") {
				require "assets/filepart/account_admin.php";
			} else {
				require "assets/filepart/account_user.php";
			}
		} else {
			echo "Sessione non settata.";
			header("Refresh: 2;URL=login.php");
		}
	?>
	<?php require "assets/filepart/footer.php"; ?>
	<!--
		Questa parte serve per poter eseguire gli script interni a Bootstrap.
		Da non modificare ed importare in ogni progetto.
		Deve rimanere sempre alla fine della pagina.
	--><script src="http://code.jquery.com/jquery.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script>
	$(document).ready(function () {
		console.log("Ready");
		var arraynum=[];

		$("#vegano").click(function(){
			if($(this).is(":checked")){
				$('#vegetariano').attr("checked",false);
			}
		});

		$("#vegetariano").click(function(){
			if($("#vegano").is(":checked")){
				$('#vegano').attr("checked",false);
			}
		});

		function alertMsg(cssclass, message) {
			$("p#alertmsg").removeClass("alert-info").removeClass("alert-success").removeClass("alert-warning").addClass(cssclass).text(message);
		}

		$('.add-ing-link').click(function(event) {
			var trovato = 0;
			event.preventDefault();
			var id = $(this).attr("href");
			var split = id.split("/");
			for(var i = 0; i < arraynum.length; i++) {
				if(arraynum[i] == split[0]) {
					trovato = 1;
				}
			}
			if(trovato == 0){
				arraynum.push(split[0]);
				var string=arraynum.toString();
				$('#hidden-text').val(string);

				var tdnome = document.createElement("td");
				$(tdnome).text(split[1]);

				var icon = document.createElement("em");
				$(icon).addClass('fa fa-trash-o').attr('aria-hidden', 'true').attr('id', split[0]);

				var link = document.createElement("a");
				$(link).addClass("delete-ingre-link").click(function(event){
				deleteIngre(event);
				}).attr('href', split[0]).append(icon).append("&nbsp;Elimina");

				var tdop = document.createElement("td");
				$(tdop).append(link);

				var trow = document.createElement("tr");
				$(trow).append(tdnome).append(tdop);
				$("tbody#agg-ing").append(trow);
				$('.delete-ingre-link');
			}
		});

		function deleteIngre(event) {
			event.preventDefault();
			var idingre = $(event.target).attr("href");
			arraynum=jQuery.grep(arraynum,function(value) {
				return value!=idingre;
			});
			var string = arraynum.toString();
			$('#hidden-text').val(string);
			$(event.target).parent().parent().remove();

		}
	
		function avanzaConsegna(event) {
		console.log("Sono dentro");
		event.preventDefault();
		var identifier = $(event.target).attr('href');
		var link = 'avanza_consegna.php?id=' + identifier;
		console.log(link);
		console.log(event.target);
		$.ajax({
			type: "POST",
			dataType: "json",
			url: link,
			success: function(data) {
			console.log("Successo");
			console.log(data);
			if(data['result'] === 'success') {
				alertMsg('alert-success','La consegna è stata avanzata correttamente al livello ' + data['consegna']);
				if(data['consegna'] === "Consegnato") {
				console.log($(event.target).parent().parent().remove());
				} else {
				$("td#stato" + identifier).text(data['consegna']);
				}
			} else if(data['result'] === 'error') {
				if(data['coderror'] === 'geterror') {
				alertMsg('alert-warning',"C'è stato un problema nel passaggio dei paramentri alla pagina di avanzamento consegna. Riprovare più tardi o contattare un amministratore di sistema.");
				} else if(data['coderror'] === 'connerr') {
				alertMsg('alert-warning', "C'è stato un problema nella connessione al server database. Riprovare più tardi o contattare un amministratore di sistema.");
				} else if(data['coderror'] === 'updatefail') {
				alertMsg('alert-warning', "C'è stato un problema nella fase di modifica dei valori sul database. Riprovare più tardi o contattare un amministratore di sistema.");
				} else if(data['coderror'] === 'consegnafatta') {
				alertMsg('alert-warning', "La consegna è già stata completata o ci sono dei dati corrotti nel database. Riprovare più tardi o contattare un amministratore di sistema.");
				} else {
				alertMsg('alert-warning', "Problema sconosciuto. Riprovare più tardi o contattare un amministratore di sistema.");
				}
			} else {
				alert("Success Unknow Error");
			}
			},
			error: function(data) {
			alert("Errore");
			console.log(data);
			}
		});
		}

		function deleteIng(event) {
		/*
		* Inibisco l'azione di default del pulsante.
		*/
		event.preventDefault();
		/*
		* Acquisizione variabili.
		*/
		var link = 'elimina_ingrediente.php?id=' + $(event.target).attr('href');
		console.log("Target");
		console.log(event.target);
		console.log($(event.target).parent());
		console.log($(event.target).parent().parent());
		console.log($(event.target).parent().parent());
		/*
		* Richiesta AJAX
		*/
		var ajaxdata = {
			"id" : $(event.target).attr('id')
		};
		$.ajax({
			type: "POST",
			dataType: "json",
			url: link,
			data: ajaxdata,
			success: function(data) {
			console.log(data);
			if(data['result'] == 'success') {
				$(event.target).parent().parent().remove();
			} else if(data['result'] == 'error') {
				alert("Success Error");
			} else {
				alert("Success Unknow Error");
			}
			},
			error: function(data){
			alert("Error");
			}
		});
		}

		function deleteProd(event) {
		/*
		* Inibisco l'azione di default del pulsante.
		*/
		event.preventDefault();
		/*
		* Acquisizione variabili.
		*/
		var link = 'elimina_prodotto.php?id=' + $(event.target).attr('href');
		console.log("Target");
		console.log(event.target);
		console.log($(event.target).parent());
		console.log($(event.target).parent().parent());
		console.log($(event.target).parent().parent());
		/*
		* Richiesta AJAX
		*/
		var ajaxdata = {
			"id" : $(event.target).attr('id')
		};
		$.ajax({
			type: "POST",
			dataType: "json",
			url: link,
			data: ajaxdata,
			success: function(data) {
			console.log(data);
			if(data['result'] == 'success') {
				$(event.target).parent().parent().remove();
			} else if(data['result'] == 'error') {
				alert("Success Error");
			} else {
				alert("Success Unknow Error");
			}
			},
			error: function(data){
			alert("Error");
			}
		});
		}

		function addIng(event) {
		/*
		* Inibisco l'azione di default del pulsante.
		*/
		event.preventDefault();
		/*
		* Acquisizione variabili.
		*/
		var nome = $("#nome").val();
		var celiaco = 0;
		var vegetariano = 0;
		var vegano = 0;
		$(":checkbox").each(function() {
			if($(this).is(':checked')) {
			if($(this).val() == "celiaco") {
				celiaco = 1;
			} else if($(this).val() == "vegetariano") {
				vegetariano = 1;
			} else if($(this).val() == "vegano") {
				vegano = 1;
			}
			}
		});
		var data = {
			"nome": nome,
			"celiaco": celiaco,
			"vegetariano": vegetariano,
			"vegano": vegano,
		};
		/*
		* Richiesta AJAX
		*/
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "aggiungi_ingrediente.php",
			data: data,
			success: function(data) {
			console.log(data);
			if(data['result'] == 'success') {
				var tdnome = document.createElement("td");
				$(tdnome).text(data['Nome']);

				var icon = document.createElement("em");
				$(icon).addClass('fa fa-trash-o').attr('aria-hidden', 'true').attr('id', data['IdIngrediente']);

				var link = document.createElement("a");
				$(link).addClass("delete-ing-link").attr('href', data['IdIngrediente']).append(icon).append("&nbsp;Elimina");

				var tdop = document.createElement("td");
				$(tdop).append(link);

				var trow = document.createElement("tr");
				$(trow).append(tdnome).append(tdop);
				$("tbody#tablebody").append(trow);
				$('.delete-ing-link').click(function(event){
				deleteIng(event);
				});

				alertMsg('alert-success', "Ingrediente inserito con successo.");
				$("#nome").val('');
			} else if(data['result'] == 'error') {
				if(data['coderror'] == '1062') {
				alertMsg("alert-warning", "Si sta tentando di aggiungere un ingrediente che è già presente.");
				} else {
				alertMsg("alert-warning", "Si sta tentando di aggiungere un ingrediente che è già presente.");
				}
			} else {
				alertMsg("alert-warning", "C'è stato un problema nel ritorno dal server, vi preghiamo di riprovare.");
			}
			},
			error: function(data){
			alertMsg("alert-warning", "C'è stato un errore nella chiamata al server, vi preghiamo di riprovare.");
			}
		});
		}

		$('#aggiungiform').submit(function(event){
			addIng(event);
		});

		$('.delete-ing-link').click(function(event){
			deleteIng(event);
		});

		$('.delete-prod-link').click(function(event){
			deleteProd(event);
		});

		$('.consegna-link').click(function(event) {
			console.log("Associati.");
			avanzaConsegna(event);
		});
	});
	</script>
</body>
</html>
