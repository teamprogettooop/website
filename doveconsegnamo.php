<!DOCTYPE html>
<html lang="it">
	<head>
		<title>Dove Consegnamo</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
		<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYquyGbk2xvYNhGDltAFVO1Or4WPLqBHY&callback=myMap"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
	</head>
	<body>
		<?php require "assets/filepart/header.php"; ?>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-12 text-center">
					<h3>Dove consegnamo</h3>
					<p>
						Consegnamo in tutta la zona di Cesena e dintorni<br/>
						Controlla se sei nel nostro bacino d'utenza
					</p>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-sm-12 text-center">
					<div class="google-maps map-wrapper text-center embed-responsive embed-responsive-4by3">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1432.0635375279212!2d12.298432658294905!3d44.12200099477705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDTCsDA3JzE5LjIiTiAxMsKwMTcnNTguMyJF!5e0!3m2!1sit!2sit!4v1583686964107!5m2!1sit!2sit" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					</div>
				</div>
			</div>
		</div>
		<?php require "assets/filepart/footer.php"; ?>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>
