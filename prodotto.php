<!DOCTYPE html>
<html lang="it">
<head>
  <title><?php
  if(!isset($_GET["Nome"])) {
    echo "Prodotto";
  } else {
    echo $_GET["Nome"];
  }
  ?></title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" /><!--
    Questo foglio di stile e lo script sono il cuore pulsante di Bootstrap.
  --><link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <!--
    Carica l'header.
  --><?php require "assets/filepart/header.php"; ?>
  <?php
    //ricava il nome del prodotto dall'URL col metodo GET.
    $prodotto = $_GET["Nome"];

    //parametri per accedere al DB.
    $host = "localhost";
    $user = "root";
    $pass = "";
    $database = "my_tentonisanzio";

    $conn = new mysqli($host, $user, $pass, $database);
    $conn->set_charset("utf8");
    if($conn->connect_error) {
        die("Connessione fallita: " . $conn->connect_error);
    }

    //prende tutte le informazioni utili alla pagina riguardo al prodotto.
    $sql = "SELECT distinct Prodotto.IdProdotto, Prodotto.Nome, Prodotto.Descrizione, Prodotto.Prezzo, Prodotto.LinkBig from (Preparato join Prodotto on Preparato.IdProdotto = Prodotto.IdProdotto) where Prodotto.Nome = '" . $prodotto . "'";
    $result = $conn->query($sql);
    while($dati = $result->fetch_assoc()) {
      $idpr = $dati["IdProdotto"];
      $nome = $dati["Nome"];
      $desc = $dati["Descrizione"];
      $prz = $dati["Prezzo"];
      $link = $dati["LinkBig"];
    }

    //prende la lista degli ingredienti del prodotto.
    $sqling = "SELECT Ingrediente.Nome as Ingrediente, Prodotto.LinkBig from (Preparato join Ingrediente on Preparato.IdIngrediente = Ingrediente.IdIngrediente) join Prodotto on Preparato.IdProdotto = Prodotto.IdProdotto where Prodotto.Nome = '" . $prodotto . "'";
    $result = $conn->query($sqling);

    if($result->num_rows === 0) {
        die("Insieme non presente!");
    } else {
      $ingredienti = [];
      while($row = $result->fetch_assoc()) {
        array_push($ingredienti, $row["Ingrediente"]);
      }
    }
    $conn->close();
   ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2><?php echo $nome;?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-sm-push-6 text-center">
        <img src="<?php echo $link;?>" class="img-responsive" id="<?php echo $nome;?>" alt="<?php echo $nome;?>">
      </div>
      <div class="col-sm-6 col-sm-pull-6">
        <h3>Descrizione</h3>
        <p><?php echo $desc;?></p>
        <h3>Ingredienti</h3>
        <p>
          <?php
            $i = 0;
            while($i < sizeof($ingredienti)) {
              if($i != 0) {
                echo ", ";
              }
              echo $ingredienti[$i];
              $i = $i + 1;
            }
            echo ".";
           ?>
        </p>
        <h3>Prezzo</h3>
        <p><?php echo $prz;?> €</p>
      </div>
    </div>
    <div class="row">
      <form method="post">
        <div class="col-sm-2 pull-right">
          <label for="aggiungi" class="sr-only">Aggiungi al carrello</label>
          <input type="image" name="aggiungi al carrello" id="aggiungi" onclick="aggiungi('<? echo $idpr; ?>')" src="assets/img/aggiungi-al-carrello.png" alt="aggiungi al carrello" width="35" height="35" >
        </div>
        <div class="col-sm-10">
          <div class="col-sm-2 pull-right">
            <label for="quantità" class="sr-only">Quantità di prodotto</label>
            <input class="form-control" name="quantità" id="quantità" type="number" min="1" max="9" value="1" title="quantità" />
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--
  carica il footer.
  --><?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script>
    //funzione che aggiunge il prodotto con la quantità scelta nel carrello.
    function aggiungi(prod) {
      var qta = document.getElementById('quantità').value;

      $.ajax({
        type: "POST",
        dataType: "json",
        url: "aggiungi_al_carrello.php",
        data: {'prod' : prod,
               'qta' : qta},
        success: function(data) {
          alert(data["result"]);
        }
      });
    }
  </script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
