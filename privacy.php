<!DOCTYPE html>
<html lang="it">
<head>
  <title>Privacy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" /><!--
      Questo foglio di stile e lo script sono il cuore pulsante di Bootstrap.
    --><link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.css" media="screen" /><script src="https://use.fontawesome.com/8c821db5ab.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <!--
  carica l'header.
  --><?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <!--
        funzione di Altervista che tramite Iubenda crea la pagina di gestione delle policy per la privacy e  cookie.
        --><a href="//www.iubenda.com/privacy-policy/27984382" class="iubenda-black no-brand iub-body-embed iubenda-embed" title="Privacy Policy">Privacy Policy</a>
        <script type="text/javascript">(function (w,d) {
          var loader = function () {
            var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
            s.src = "//cdn.iubenda.com/iubenda.js";
            tag.parentNode.insertBefore(s,tag);
          };
          if(w.addEventListener){
            w.addEventListener("load", loader, false);
          }else if(w.attachEvent){
            w.attachEvent("onload", loader);
          }else{
            w.onload = loader;
          }
        })(window, document);
        </script>
      </div>
    </div>
  </div>
  <!--
  carica il footer.
  --><?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
