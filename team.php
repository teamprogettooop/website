<!DOCTYPE html>
<html lang="it">
	<head>
		<title>Team</title>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
		<!--<link rel="stylesheet" type="text/css" href="assets/css/team.css" media="screen" />-->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYquyGbk2xvYNhGDltAFVO1Or4WPLqBHY&callback=myMap"></script>
	</head>
	<body>
		<?php require "assets/filepart/header.php"; ?>
		<div class="container team">
			<div class="row align-items-center">
				<div class="col-sm-12 text-center">
					<div class="title page-header">
						<h2>Questo è il nostro TEAM!</h2>
						<p class="lead">Siamo una piccola azienda di commercio di frutta e verdura. Abbiamo il nostro principale punto di vendita al Mercato Coperto Centrale di Rimini, ma possiamo eventualmente anche metterci d'accordo per ricevere i clienti direttamente a casa nostra, dove abbiamo anche una piccola zona di produzione. Siamo stati aiutati da un piccolo gruppo di studenti universitari laureandi in Ingegneria e Scienze Informatiche a Cesena nella realizzazione del sito.<br />Del territorio per il territorio.</p>
					</div>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4">
					<img src="assets/img/sanzio_e_paola_2.jpg" class="img-thumbnail" alt="TENTONI SANZIO">
				</div>
				<div class="col-sm-8 text-center">
					<h3>TENTONI SANZIO</h3>
					<p> Sono il fondatore dell'azienda e sono fiero di essere un fruttivendolo!.</p>
					<p> Sono felicemente sposato con Paola.</p>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4 col-sm-push-8">
					<img src="assets/img/sanzio_e_paola_1.jpg" class="img-thumbnail" alt="BOTTEGHI PAOLA">
				</div>
				<div class="col-sm-8 col-sm-pull-4 text-center">
					<h3>BOTTEGHI PAOLA</h3>
					<p> Sono la fondatrice dell'azienda e sono fiera di essere una fruttivendola!.</p>
					<p> Sono felicemente sposata con Sanzio.</p>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4">
					<img src="assets/img/monda.png" class="img-thumbnail" alt="MONDAINI LUCA">
				</div>
				<div class="col-sm-8 text-center">
					<h3>MONDAINI LUCA</h3>
					<p> Luca Mondaini è uno dei 3 creatori di questo sito, classe 1996.</p>
					<p> Frequenta il terzo anno ed è proprio bravo!</p>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4 col-sm-push-8">
					<img src="assets/img/pracu.png" class="img-thumbnail" alt="PRACUCCI NICOLO'">
				</div>
				<div class="col-sm-8 col-sm-pull-4 text-center">
					<h3>PRACUCCI NICOLO'</h3>
					<p>Anche lui classe 96, gioca a pallavolo come palleggiatore nel Gambettola.(Fa parte del team anche se Gambettola è un comune a parte).</p>
					<p>Frequenta anche lui il terzo anno di Ingegneria Informatica a Cesena insieme a noi.
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4">
					<img src="assets/img/tento.png" class="img-thumbnail" alt="TENTONI DANIELE">
				</div>
				<div class="col-sm-8 text-center" style="padding-bottom: 40px;">
					<h3>TENTONI DANIELE</h3>
					<p>Capo indiscusso di questo team, visto i suoi precedenti anni di scoutismo, classe 1996, miglior programmatore in circolazione.</p>
					<p>Frequenta anche lui il terzo anno di Ingegneria Informatica a Cesena insieme a noi.
				</div>
			</div>
		</div>
		<!--<div class="container fornitori">
			<div class="row align-items-center title">
				<div class="col-sm-12 text-center">
					<h3> Questi sono i nostri FORNITORI!</h3>
					<p> Sono tutti della zona e ci tengono molto al kilometro zero, alla qualità e alla convenienza.</p>
					<p> Una spalla su cui contare.</p>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4">
					<img src="assets/img/conad.jpg" class="img-thumbnail" alt="Responsive image">
				</div>
				<div class="col-sm-8 text-center" style="padding-bottom: 120px;">
					<h3> CONAD </h3>
					<p> E' il nostro miglior fornitore da circa 10 anni, mai avuto problemi, consigliatissimo! </p>
					<p> Provare per credere. </p>
				</div>
			</div>
		</div>
		<div class="container corrieri">
			<div class="row align-items-center title">
				<div class="col-sm-12 text-center">
					<h3> Questi sono i nostri CORRIERI!</h3>
					<p> Velocità e affidabilità.</p>
				</div>
			</div>
			<div class="row teammates">
				<div class="col-sm-4">
					<img src="assets/img/dhl.jpg" class="img-thumbnail" alt="Responsive image">
				</div>
				<div class="col-sm-8 text-center">
					<h3> DHL </h3>
					<p> E' il nostro miglior corriere da circa 6 anni, mai avuto problemi, consigliatissimo! </p>
					<p> Provare per credere. </p>
				</div>
			</div>
		</div>-->
		<?php require "assets/filepart/footer.php"; ?>
	</body>
</html>
