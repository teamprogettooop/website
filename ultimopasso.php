<!DOCTYPE html>
<html lang="it">
	<head>
		<title>Ancora un ultimo passo</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
	</head>
	<body>
	  <?php require "assets/filepart/header.php"; ?>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
          <?php
            if($user["Titolo"] != "Amministratore") {
              require "assets/filepart/pannello_controllo_part.php";
            } else {
              require "assets/filepart/pannello_controllo_admin_part.php";
            }
          ?>
				</div>
				<div class="col-sm-9">
					<div class="row align-items-center">
						<div class="col-sm-12 text-center">
							<h3>Ancora un ultimo passo!</h3>
							<p> Ci siamo quasi, ci servono le ultime informazioni per far partire il tuo ordine!</p>
						</div>
					</div>
					<form name="formPasso" method="post" autocomplete="on">
						<div class="form-group row align-items-left">
							<label for="indirizzo" class="col-form-label col-sm-3">Indirizzo di Spedizione</label>
							<div class="col-sm-12 text-left">
								<input type="text" class="form-control" name="indirizzo" id="indirizzo" placeholder="Indirizzo di spedizione" required/>
								<small id="smallIndirizzo" class="form-text text-muted">Non condivideremo la sua via con nessuno.</small>
								<h4><strong>ATTENZIONE! Assicurati che l'indirizzo di spedizione sia compreso nel nostro range di spedizioni! Puoi farlo <a href="doveconsegnamo.php">cliccando qui</a></strong></h4>
							</div>
						</div>
						<div class="form-group row align-items-left">
							<label for="cc" class="col-form-label col-sm-3">Numero Carta di Credito</label>
							<div class="col-sm-12 text-left">
								<input type="number" class="form-control" name="cc" id="cc" placeholder="Numero Carta di Credito" required/>
								<small id="ccTip" class="form-text text-muted">Non condivideremo il suo numero di carta con nessuno.</small>
							</div>
						</div>
						<div class= "form-group row align-items-left">
							  <div class="col-sm-12 text-left">
									<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Procedi con l'ordine e paga"/>
							  </div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php require "assets/filepart/footer.php"?>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script>
		$(document).ready(function() {
			$("form").submit(function() {

            	var lung = $("input[name='cc']").val();
                if(lung.length!='16'){
                	alert("Inserire correttamente le 16 cifre");
                }else{

                  //event.target, l'elemento su cui l'utente ha cliccato
                  var $target = $(event.target);

                  //inibisce il comportamento standard del link
                  event.preventDefault();

                  var data = {
                      "action": $target.attr('class'),
                  };
                  data = $(this).serialize() + "&" + $.param(data);

                  var dest = document.getElementById('indirizzo').value;

                  //Richiesta AJAX
                  $.ajax({
                      type: "POST",
                      url: "insert_into_storico.php",
                      data: {'dest' : dest},
                      success: function(data) {
						alert("Acquisto effettuato! Grazie per aver usato il nostro servizio");
						location.href = '/index.php';
						console.log("ok"+data);
                      },
                      error: function(data){
						alert("Qualcosa è andato storto, contatta gli amministratori");
						console.log("error"+data);
                      }
                  });
               	}
			});
		});
		</script>
	</body>
</html>
