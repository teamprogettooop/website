<?php
$esito = "positivo";
if(!isset($_POST["email"])) {
  $esito = "negativo";
} else {
  $conn = new mysqli("localhost", "root", "", "my_tentonisanzio");
  $conn->set_charset("utf8");
  if($conn->connect_errno) {
    $esito = "negativo";
  } else {
    $sql = "SELECT Password FROM Utente WHERE Email = ? LIMIT 1";
    $query = $conn->prepare($sql);
    $query->bind_param('s', $usemail);
    $usemail = mysql_real_escape_string($_POST["email"]);
    $query->execute();
    $result = $query->get_result();
    if($result->num_rows > 0) {
      $user = $result->fetch_array(MYSQLI_ASSOC);
      $conn->close();
      $to = $usemail;
      $email = "apenzapina@altervista.org";
      $oggetto = "Recupero Password";
      $messaggio = "<html>";
      $messaggio.= "<head>";
      $messaggio.= "  <title>Conferma Email</title>";
      $messaggio.= "</head>";
      $messaggio.= "<body>";
      $messaggio.= "  <h1>Benvenuto su A Penza Pina!</h1>";
      $messaggio.= "  <p>Cliccando sul link sottostante potrai impostare una nuova password di login.</p>";
      $messaggio.= '  <span>Clicca sul link per poter compilare il form di richiesta di nuova password. <a href="http://www.apenzapina.altervista.org/impostapassword.php?mail=' . $usemail . '&codicerecupero=' . $user["Password"] . '">http://www.apenzapina.altervista.org/impostapassword.php?mail=' . $usemail . '&codicerecupero=' . $user["Password"] . '</a></span>';
      $messaggio.= '  <p>Se il link non funziona, incollalo nella barra degli indirizzi del tuo browser.</p>';
      $messaggio.= "  <p>Se non hai richiesto tu il recupero password presso il nostro sito, ti preghiamo di ignorare questa email ed eliminarla.</p>";
      $messaggio.= '  <p>In caso di bisogno, non rispondere direttamente a questa email, utilizzare direttamente il modulo <a href="apenzapina.altervista.org/contattaci.php">qui.</a></p>';
      $messaggio.= "</body>";
      $messaggio.= "</html>";
      $headers .= "From: APenzaPina <apenzapina@altervista.org>\r\n";
      $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
      $headers =  "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
      $headers .= "Content-Transfer-Encoding: 7bit\n\n";
      if( mail($to, $oggetto, $messaggio, $headers) ) {
        $esito = "positivo";
      } else {
        $esito = "negativo";
      }
    }
  }
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title>Recupero Password</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
  <script src="https://use.fontawesome.com/8c821db5ab.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
</head>
<body>
  <?php require "assets/filepart/header.php"; ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Recupero Password</h3>
          </div>
          <div class="panel-body">
            <?php
            if(isset($esito) && $esito == "positivo") {
              echo '<div class="alert alert-success" role="alert">Email inviata a: ' . $to . ', speriamo che arrivi.</div>';
            } else {
              echo '<div class="alert alert-warning">';
              echo "L'invio dell'email non è andato a buon fine. Ciò può essere dovuto ad un problema con l'invio dei dati per email. Tornare indietro e riprovare ad eseguire la procedura. Per ultimo, richiedere l'intervento di un amministratore di sistema <a href='contattaci.php' class='alert-link'>cliccando qui.</a>";
              echo '</div>';
            }
             ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require "assets/filepart/footer.php"; ?>
  <!--
    Questa parte serve per poter eseguire gli script interni a Bootstrap.
    Da non modificare ed importare in ogni progetto.
    Deve rimanere sempre alla fine della pagina.
  --><script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>
