 <!DOCTYPE html>
<html lang="it">
	<head>
		<title>Prodotti</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<script src="https://use.fontawesome.com/8c821db5ab.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="assets/css/main.css" media="screen" />
	</head>
	<body>
		<?php
		require "assets/filepart/header.php";
		if(isset($_SESSION["login_user"])) {
			$user = $_SESSION["login_user"];
		}
    	?>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="antipasti">Antipasti</a></li>
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="primi">Primi</a></li>
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="secondi">Secondi</a></li>
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="contorni">Contorni</a></li>
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="dolce">Dolce</a></li>
						<li class="toolbar col-xs-4 col-sm-2 text-center" role="presentation"><a href="#" class="frutta">Frutta</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
          <div class="well well-sm">
            <div class="row"><!--
              Checkbox Celiaco.
            --><div class="col-sm-4">
                <div class="input-group">
                  <span class="input-group-addon">
                    <input type="checkbox" class="check cel" value="celiaco" id="celiaco" name="celiaco" aria-label="Checkbox per celiaco" <?php
                    if(isset($user) && $user["Celiaco"] == "1") {
                      echo "checked";
                    }
                    ?> />
                  </span>
                  <label for="celiaco" class="form-control" aria-label="Testo label per checkbox Celiaco" >Celiaco</label>
                </div>
              </div><!--
                Checkbox Vegano.
              --><div class="col-sm-4">
                <div class="input-group">
                  <span class="input-group-addon">
                    <input type="checkbox" class="check vegan" value="vegano" id="vegano" name="vegano" aria-label="Checkbox per vegano" <?php
                    if(isset($user) && $user["Vegano"] == "1") {
                      echo "checked";
                    }
                    ?> />
                  </span>
                  <label for="vegano" class="form-control" aria-label="Testo label per checkbox Vegano" >Vegano</label>
                </div>
              </div><!--
                Checkbox Vegetariano.
              --><div class="col-sm-4">
                <div class="input-group">
                  <span class="input-group-addon">
                    <input type="checkbox" class="check veg" value="vegetariano" id="vegetariano" name="vegetariano" aria-label="Checkbox per vegetariano" <?php
                    if(isset($user) && $user["Vegetariano"] == "1") {
                      echo "checked";
                    }
                    ?> />
                  </span>
                  <label for="vegetariano" class="form-control" aria-label="Testo label per checkbox Vegetariano" >Vegetariano</label>
                </div>
              </div>
            </div>
          </div>
				</div>
			</div>
			<div id="prodotti" class="row">

			</div>
		</div>
		<?php


		?>
		<?php
			 require "assets/filepart/footer.php";
		?>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
		<script>
		$(document).ready(function () {
			//Funzione che richiamiamo quando la pagina viene caricata.
			primaVolta();


			$("#vegano").click(function(){
				if($(this).is(":checked")){
					$('#vegetariano').attr("checked",false);
				}
			});

			$("#vegetariano").click(function(){
				if($("#vegano").is(":checked")){
					$('#vegano').attr("checked",false);
				}
			});

			//Quando viene cliccato un link si attiva questa funzione.
			$('.toolbar').click(function (event) {
				var celiaco = 0;
				var vegetariano = 0;
				var vegano = 0;
				//Controllo di ogni checkbox.
				$(":checkbox").each(function() {
				  if($(this).is(':checked')) {
					if($(this).val() == "celiaco") {
					  celiaco = 1;
					} else if($(this).val() == "vegetariano") {
					  vegetariano = 1;
					} else if($(this).val() == "vegano") {
					  vegano = 1;
					}
				  }
				});

				//Inibisco l'azione di default del pulsante.
				var $target = $(event.target);
				event.preventDefault();

				//Preparazione variabile da inviare con ajax.
				var data = {
					"action": $target.attr('class'),
					"celiaco": celiaco,
					"vegetariano": vegetariano,
					"vegano": vegano,
				};

				//Richiesta AJAX.
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "prodottiquery.php",
					data: data,
					//In caso di successo
					success: function(data) {
						//Pulisco tutti i prodotti.
						var prodotti = $("div#prodotti");
						$(prodotti).empty();
						//Splitto i prodotti avuti dalla chiamata ajax.
						var res=data["pasti"].split('^');
						for(var i = 0; i < res.length - 1; i++) {
							//Per ogni prodotto splitto le sue caratteristiche.
							var element = res[i].split('*');
							//Nome del prodotto
							var title = document.createElement("h4");
							$(title).text(element[0]).addClass("text-center");
							//Creazione div e ci inserisco il nome.
							var divcaption = document.createElement("div");
							$(divcaption).append(title);
							//Creo immagine, ci metto alt, classi e src.
							var img = document.createElement("img");
							$(img).addClass("img-responsive").attr("src",element[1]).attr("alt", $target.attr('class') + " " + element[0]);
							//Creazione a con href, al click manderà informazioni a prodotto.php.
							var link = document.createElement("a");
							$(link).append(img).attr("href", "prodotto.php?Nome=" + element[0]);
							var thumbnail = document.createElement("div");
							$(thumbnail).addClass("thumbnail").append(link).append(divcaption);
							var colonna = document.createElement("div");
							$(colonna).addClass("col-xs-6 col-sm-4 col-md-3").append(thumbnail);
							prodotti.append(colonna);
						}
					}
				});

			});

			//Funzione primaVolta() che carica gli antipasti.
			function primaVolta(){
				var celiaco = 0;
				var vegetariano = 0;
				var vegano = 0;
				 $(":checkbox").each(function() {
				  if($(this).is(':checked')) {
					if($(this).val() == "celiaco") {
					  celiaco = 1;
					} else if($(this).val() == "vegetariano") {
					  vegetariano = 1;
					} else if($(this).val() == "vegano") {
					  vegano = 1;
					}
				  }
				});

				var data = {
					"action": "antipasti",
					"celiaco": celiaco,
					"vegetariano": vegetariano,
					"vegano": vegano,
				};

				$.ajax({
					type: "POST",
					dataType: "json",
					url: "prodottiquery.php",
					data: data,
					success: function(data) {
						var prodotti = $("div#prodotti");
						$(prodotti).empty();
						var res=data["pasti"].split('^');
						for(var i = 0; i < res.length - 1; i++) {
						  var element = res[i].split('*');
						  var title = document.createElement("h4");
						  $(title).text(element[0]).addClass("text-center");
						  var divcaption = document.createElement("div");
						  $(divcaption).append(title);
						  var img = document.createElement("img");
						  $(img).addClass("img-responsive").attr("src",element[1]);
						  var link = document.createElement("a");
						  $(link).append(img).attr("href", "prodotto.php?Nome=" + element[0]);
						  var thumbnail = document.createElement("div");
						  $(thumbnail).addClass("thumbnail").append(link).append(divcaption);
						  var colonna = document.createElement("div");
						  $(colonna).addClass("col-xs-6 col-sm-4 col-md-3").append(thumbnail);
						  prodotti.append(colonna);
						}
					}
				});
			}
		});
		</script>
	</body>
</html>
